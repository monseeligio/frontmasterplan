import { Component } from '@angular/core';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  datosLogin = { emailUsuario: '', passwordUsuario: '' };

  constructor(private _service: ApirestService, private _router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('token') != null) {
      this._router.navigate(['menuPrincipal']);
    }
  }
  login() {
    if (this.datosLogin.emailUsuario == '' || this.datosLogin.passwordUsuario == '') {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Por favor, llena todos los campos.'
      });
    } else {
      this._service.login(this.datosLogin).subscribe((resp: any) => {
        localStorage.setItem('token', resp.token);
        this._router.navigate(['menuPrincipal'])
      }, (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: error.error?.mensaje || 'Error en la autenticación.'
        });

      })
    }
  }

}
