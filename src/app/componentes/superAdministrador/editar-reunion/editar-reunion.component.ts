import { Component, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import Swal from 'sweetalert2';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-editar-reunion',
  templateUrl: './editar-reunion.component.html',
  styleUrls: ['./editar-reunion.component.css']
})
export class EditarReunionComponent {
  @ViewChild('agregarPaginaModal', { static: false }) agregarPaginaModal!: TemplateRef<any>;
  @ViewChild('editarPaginaModal', { static: false }) editarPaginaModal!: TemplateRef<any>;

  idReunion: any
  emailUsuario: any
  idUsuario: any
  reunion = { nombreReunion: '', idReunion: '' }
  datosUsuario: any;
  integrantesReunion: any[] = [];

  paginasReunion = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };
  paginasReunionAct = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };

  paginas: any[] = [];

  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private _modal: NgbModal, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.obtenerDetalles()
  }

  async obtenerDetalles() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunion'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunion = 'R' + idNumero;
      const token: any = localStorage.getItem('token');
      const tokenPayload: any = jwt_decode(token);
      this.emailUsuario = tokenPayload.emailUsuario;
      this.datosUsuario = (await this._service.obtenerUsuarios().toPromise()) as any[];
      this.datosUsuario = this.datosUsuario.filter((usuario: any) => usuario.statusUsuario === 'Activo');
      this.idUsuario = await this._service.obtenerIDUsuario(this.emailUsuario).toPromise();
      this._reunion.obtenerDatosReunion(this.idReunion).subscribe((resp: any) => {
        this.reunion.nombreReunion = resp[0].nombreReunion;
        this.integrantesReunion = resp.map((usuario: any) => usuario.idUsuario);
      });
      this._reunion.obtenerPaginas(this.idReunion).subscribe((resp: any) => {
        this.paginas = resp;
      });
    });
  }


  abrirNuevaPaginaModal() {
    this._modal.open(this.agregarPaginaModal);
  }

  agregarPagina() {
    if (this.paginasReunion.tituloPagina === '' ||
      this.paginasReunion.tipoPagina === '' ||
      this.paginasReunion.duracionPagina === '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.paginasReunion.ordenPagina = this.paginas.length
      this.paginasReunion.idReunion = this.idReunion
      const nuevaPagina = { ...this.paginasReunion };
      this.paginas.push(nuevaPagina);
      this.paginasReunion = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };
      this.cerrarModal();
    }
  }

  abrirEditarPagina(data: any, index: any) {
    this._modal.open(this.editarPaginaModal);
    this.paginasReunionAct.ordenPagina = index;
    this.paginasReunionAct.tituloPagina = data.tituloPagina;
    this.paginasReunionAct.tipoPagina = data.tipoPagina;
    this.paginasReunionAct.urlExternaPagina = data.urlExternaPagina;
    this.paginasReunionAct.duracionPagina = data.duracionPagina;
  }

  actualizarPagina() {
    this.paginas[this.paginasReunionAct.ordenPagina] = this.paginasReunionAct
    this.paginasReunionAct = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };

    this.cerrarModal()
  }

  eliminarPagina(index: number) {
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'Esta acción no se puede deshacer.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.paginas.splice(index, 1);
      }
    });
  }

  async editarReunion() {
    try {
      if (this.integrantesReunion.length === 0 || this.paginas.length === 0 || this.reunion.nombreReunion === '') {
        Swal.fire({
          icon: 'error',
          title: '¡Error!',
          text: 'Llene todos los campos.',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar'
        });
      } else {
        this.reunion.idReunion = this.idReunion
        this._reunion.actualizarNombreReunion(this.reunion).toPromise()
        this._reunion.borrarIntegrantes(this.idReunion).toPromise()
        for (const integrante of this.integrantesReunion) {
          let data = {idUsuario:integrante, idReunion:this.idReunion}
          await this._reunion.integrantes(data).toPromise();
        }
        this._reunion.borrarPaginas(this.idReunion).toPromise()
        for (const pagina of this.paginas) {
          pagina.idReunion = this.idReunion
          await this._reunion.paginas(pagina).toPromise();
        }
        Swal.fire({
          icon: 'success',
          title: '¡Reunión editada con éxito!',
          showConfirmButton: false,
          timer: 1500
        });
        this._router.navigate(['/reuniones']);
      }
    } catch (err) {
      alert('Ocurrió un error al crear la reunión');
      console.error(err);
    }
  }

  cerrarModal() {
    this._modal.dismissAll();
  }

}
