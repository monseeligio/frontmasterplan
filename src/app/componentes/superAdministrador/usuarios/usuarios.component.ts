import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApirestService } from 'src/app/servicios/apirest.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  @ViewChild('agregarUsuarioModal', { static: false }) agregarUsuarioModal!: TemplateRef<any>;
  @ViewChild('editarUsuarioModal', { static: false }) editarUsuarioModal!: TemplateRef<any>;
  @ViewChild('eliminarUsuarioModal', { static: false }) eliminarUsuarioModal!: TemplateRef<any>;

  datosUsuario: any = [];
  jefes: any
  datosUsuarioCrear = {
    nombreUsuario: '', apellidoUsuario: '', emailUsuario: '', passwordUsuario: '', puestoUsuario: '', jefeDirectoUsuario: '', roleUsuario: '', statusUsuario: 'Activo', imagenUsuario: 'No'
  };
  datosUsuarioActualizar = {
    idUsuario: '', nombreUsuario: '', apellidoUsuario: '', emailUsuario: '', passwordUsuario: '', puestoUsuario: '', jefeDirectoUsuario: '', roleUsuario: '', statusUsuario: ''
  };
  datosViejosUsuarioActualizar = {
    idUsuario: '', nombreUsuario: '', apellidoUsuario: '', emailUsuario: '', passwordUsuario: '', puestoUsuario: '', jefeDirectoUsuario: '', roleUsuario: '', statusUsuario: ''
  };


  constructor(private _service: ApirestService, private _router: Router, private _modal: NgbModal) { }

  ngOnInit(): void {
    this.obtenerUsuario()
  }

  obtenerUsuario() {
    this._service.obtenerUsuarios()
      .subscribe((resp: any) => {
        this.datosUsuario = resp;
        function compare(a: any, b: any): number {
          return a.idUsuario.split("U")[1] - b.idUsuario.split("U")[1];
        }
        this.datosUsuario.sort(compare);
        this.jefes = this.datosUsuario.filter((usuario: any) => usuario.roleUsuario === 'Superadministrador' || usuario.roleUsuario === 'Administrador');
      });

  }

  abrirNuevoUsuarioModal() {
    this._modal.open(this.agregarUsuarioModal);
  }

  agregarUsuario() {
    this._service.crearUsuario(this.datosUsuarioCrear).subscribe((resp: any) => {
      this.obtenerUsuario();
      this.datosUsuarioCrear = {
        nombreUsuario: '', apellidoUsuario: '', emailUsuario: '', passwordUsuario: '', puestoUsuario: '', jefeDirectoUsuario: '', roleUsuario: '', statusUsuario: 'Activo', imagenUsuario: 'No'
      };
      this._modal.dismissAll();
    }, (error) => {
      if (error.error.error) {
        if (error.error.error.includes("empty")) {
          console.log(error.error);
          alert('Llene los campos');
        }
        if (error.error.error.includes("valid email")) {
          alert('Ingrese un correo válido');
        }
        if (error.error.error.includes("at least")) {
          alert('La contraseña debe tener al menos 6 caracteres');
        }
      }
      else if (error.error.mensaje.includes("registrado")) {
        alert('El email está ya existe, ingrese otro');
      }
    })

  }

  // Función para abrir el modal de editar Usuario
  abrirEditarUsuarioModal(id: any) {
    this._modal.open(this.editarUsuarioModal);
    this.datosUsuarioActualizar.idUsuario = id.idUsuario
    this.datosUsuarioActualizar.nombreUsuario = id.nombreUsuario
    this.datosUsuarioActualizar.apellidoUsuario = id.apellidoUsuario
    this.datosUsuarioActualizar.emailUsuario = id.emailUsuario
    this.datosUsuarioActualizar.passwordUsuario = id.passwordUsuario
    this.datosUsuarioActualizar.puestoUsuario = id.puestoUsuario
    this.datosUsuarioActualizar.jefeDirectoUsuario = id.jefeDirectoUsuario
    this.datosUsuarioActualizar.roleUsuario = id.roleUsuario
    this.datosUsuarioActualizar.statusUsuario = id.statusUsuario
    this.datosViejosUsuarioActualizar.idUsuario = id.idUsuario
    this.datosViejosUsuarioActualizar.nombreUsuario = id.nombreUsuario
    this.datosViejosUsuarioActualizar.apellidoUsuario = id.apellidoUsuario
    this.datosViejosUsuarioActualizar.emailUsuario = id.emailUsuario
    this.datosViejosUsuarioActualizar.passwordUsuario = id.passwordUsuario
    this.datosViejosUsuarioActualizar.puestoUsuario = id.puestoUsuario
    this.datosViejosUsuarioActualizar.jefeDirectoUsuario = id.jefeDirectoUsuario
    this.datosViejosUsuarioActualizar.roleUsuario = id.roleUsuario
    this.datosViejosUsuarioActualizar.statusUsuario = id.statusUsuario

  }

  editarUsuario() {
    if (this.datosViejosUsuarioActualizar.idUsuario == this.datosUsuarioActualizar.idUsuario &&
      this.datosViejosUsuarioActualizar.nombreUsuario == this.datosUsuarioActualizar.nombreUsuario &&
      this.datosViejosUsuarioActualizar.apellidoUsuario == this.datosUsuarioActualizar.apellidoUsuario &&
      this.datosViejosUsuarioActualizar.emailUsuario == this.datosUsuarioActualizar.emailUsuario &&
      this.datosViejosUsuarioActualizar.passwordUsuario == this.datosUsuarioActualizar.passwordUsuario &&
      this.datosViejosUsuarioActualizar.roleUsuario == this.datosUsuarioActualizar.roleUsuario &&
      this.datosViejosUsuarioActualizar.statusUsuario == this.datosUsuarioActualizar.statusUsuario &&
      this.datosViejosUsuarioActualizar.puestoUsuario == this.datosUsuarioActualizar.puestoUsuario &&
      this.datosViejosUsuarioActualizar.jefeDirectoUsuario == this.datosUsuarioActualizar.jefeDirectoUsuario) {
      this._modal.dismissAll()
    }
    else if (this.datosViejosUsuarioActualizar.emailUsuario == this.datosUsuarioActualizar.emailUsuario) {
      this._service.actualizarUsuario(this.datosUsuarioActualizar).subscribe((resp: any) => {
        this.obtenerUsuario()
        this._modal.dismissAll()
      }, (error => {
        if (error.error.error) {
          if (error.error.error.includes("empty")) {
            alert('Llene los campos');
          }
          if (error.error.error.includes("valid email")) {
            alert('Ingrese un correo válido');
          }
          if (error.error.error.includes("at least")) {
            alert('La contraseña debe tener al menos 6 caracteres');
          }
        }
      }))
    }
    else if (this.datosViejosUsuarioActualizar.emailUsuario != this.datosUsuarioActualizar.emailUsuario) {
      this._service.actualizarUsuarioEmail(this.datosUsuarioActualizar).subscribe((resp: any) => {
        this.obtenerUsuario();
        this._modal.dismissAll();
      }, (error) => {
        if (error.error.error) {
          if (error.error.error.includes("empty")) {
            alert('Llene los campos');
          }
          if (error.error.error.includes("valid email")) {
            alert('Ingrese un correo válido');
          }
          if (error.error.error.includes("at least")) {
            alert('La contraseña debe tener al menos 6 caracteres');
          }
        }
        else if (error.error.mensaje.includes("registrado")) {
          alert('El email está ya existe, ingrese otro');
        }
      })
    }
  }

  // Función para abrir el modal de eliminar Usuario
  abrirEliminarUsuarioModal() {
    this._modal.open(this.eliminarUsuarioModal);
  }

  // Función para eliminar un Usuario
  eliminarUsuario(id: any) {
    Swal.fire({
      title: '¿Seguro que desea eliminar este usuario?',
      text: "¡No podrás revertir este cambio!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar'
    }).then((result) => {
      if (result.isConfirmed) {
        this._service.eliminarUsuario(id).subscribe((resp: any) => {
          this.obtenerUsuario()
        });
        Swal.fire(
          'Eliminado!',
          'Usuario eliminado con éxito',
          'success'
        )
        this.obtenerUsuario()
      }
    })
    this._modal.dismissAll();
  }

  cerrarModal() {
    this._modal.dismissAll(); // Cierra todos los modales abiertos
  }

}
