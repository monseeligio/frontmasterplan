import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import Swal from 'sweetalert2';
import jwt_decode from "jwt-decode";
import { DragAndDropModule } from 'angular-draggable-droppable';

@Component({
  selector: 'app-crear-reunion',
  templateUrl: './crear-reunion.component.html',
  styleUrls: ['./crear-reunion.component.css']
})
export class CrearReunionComponent implements OnInit {
  @ViewChild('agregarPaginaModal', { static: false }) agregarPaginaModal!: TemplateRef<any>;
  @ViewChild('editarPaginaModal', { static: false }) editarPaginaModal!: TemplateRef<any>;

  emailUsuario: any
  idUsuario: any
  reunion = { nombreReunion: '', statusReunion: 'Activa' }
  datosUsuario: any;
  integrantesReunion: any[] = [];
  datosReunion: any = {};

  paginasReunion = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };
  paginasReunionAct = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };

  paginas: any[] = [];

  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private _modal: NgbModal) { }

  ngOnInit(): void {
    this.obtenerUsuario();
  }

  async obtenerUsuario() {
    const token: any = localStorage.getItem('token');
    const tokenPayload: any = jwt_decode(token);
    this.emailUsuario = tokenPayload.emailUsuario;
    this.datosUsuario = (await this._service.obtenerUsuarios().toPromise()) as any[];
    this.datosUsuario = this.datosUsuario.filter((usuario: any) => usuario.statusUsuario === 'Activo');
    this.idUsuario = await this._service.obtenerIDUsuario(this.emailUsuario).toPromise();
  }

  abrirNuevaPaginaModal() {
    this._modal.open(this.agregarPaginaModal);
  }

  async crearReunion() {
    this.idUsuario = await this._service.obtenerIDUsuario(this.emailUsuario).toPromise();
    this.integrantesReunion.push(this.idUsuario[0].idUsuario);
    try {
      if (this.integrantesReunion.length === 0 || this.paginas.length === 0 || this.reunion.nombreReunion === '') {
        Swal.fire({
          icon: 'error',
          title: '¡Error!',
          text: 'Llene todos los campos.',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar'
        });
      } else {
        const resp: any = await this._reunion.crearReunion(this.reunion).toPromise();
        this.datosReunion.idReunion = resp.idReunion;
        for (const integrante of this.integrantesReunion) {
          this.datosReunion.idUsuario = integrante;
          await this._reunion.integrantes(this.datosReunion).toPromise();
        }
        for (const pagina of this.paginas) {
          pagina.idReunion = this.datosReunion.idReunion
          await this._reunion.paginas(pagina).toPromise();
        }
        Swal.fire({
          icon: 'success',
          title: '¡Reunión creada exitosamente!',
          showConfirmButton: false,
          timer: 1500
        });
        this._router.navigate(['/reuniones']);
      }
    } catch (err) {
      alert('Ocurrió un error al crear la reunión');
      console.error(err);
    }
  }

  agregarPagina() {
    if (this.paginasReunion.tituloPagina === '' ||
      this.paginasReunion.tipoPagina === '' ||
      this.paginasReunion.duracionPagina === '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.paginasReunion.ordenPagina = this.paginas.length
      const nuevaPagina = { ...this.paginasReunion };
      this.paginas.push(nuevaPagina);
      this.paginasReunion = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: this.datosReunion.idReunion };
      this.cerrarModal();
    }
  }

  abrirEditarPagina(data: any, index: any) {
    this._modal.open(this.editarPaginaModal);
    this.paginasReunionAct.ordenPagina = index;
    this.paginasReunionAct.tituloPagina = data.tituloPagina;
    this.paginasReunionAct.tipoPagina = data.tipoPagina;
    this.paginasReunionAct.urlExternaPagina = data.urlExternaPagina;
    this.paginasReunionAct.duracionPagina = data.duracionPagina;
  }

  actualizarPagina() {
    this.paginas[this.paginasReunionAct.ordenPagina] = this.paginasReunionAct
    this.paginasReunionAct = { ordenPagina: 0, tituloPagina: '', tipoPagina: '', urlExternaPagina: '', duracionPagina: '', idReunion: '' };

    this.cerrarModal()
  }

  eliminarPagina(index: number) {
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'Esta acción no se puede deshacer.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.paginas.splice(index, 1);
      }
    });
  }

  cerrarModal() {
    this._modal.dismissAll();
  }
}
