import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import jwt_decode from "jwt-decode";
import { format, startOfWeek, endOfWeek, parse } from 'date-fns';
import { es } from 'date-fns/locale';
import Swal from 'sweetalert2';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { firstValueFrom } from 'rxjs';


@Component({
  selector: 'app-menu-principal',
  templateUrl: './menu-principal.component.html',
  styleUrls: ['./menu-principal.component.css']
})
export class MenuPrincipalComponent {

  idUsuario: any
  nombreUsuario: String = ''
  apellidoUsuario: String = ''
  actividades: { [nombreReunion: string]: any[] } = {};
  todasActividades: any[] = [];
  aux: boolean = false
  indicadores: { [nombreReunion: string]: any[] } = {};
  aux1: boolean = false
  semana: any;
  semanaCalcular: any
  datos: any = []
  fechas: any[][] = [];
  fechasRestantes: any[][] = []
  fechasRestantesTabla: any[][] = []
  fechasReverso: any[][] = [];
  fechasFormateadas: any[][] = [];
  fechasFormateadasReverso: any[][] = [];
  data = { idMeasurable: '', fechaMeasurable: '' };
  resultado: any
  resultados: any = {}

  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.obtenerRangoSemana()
    this.obtenerSesion()
  }

  obtenerRangoSemana(): string {
    const today = new Date();
    const firstDayOfWeek = startOfWeek(today, { weekStartsOn: 1 });
    const lastDayOfWeek = endOfWeek(today, { weekStartsOn: 1 });
    const formattedFirstDay = format(firstDayOfWeek, 'MMM d', { locale: es });
    const formattedLastDay = format(lastDayOfWeek, 'MMM d', { locale: es });
    const auxformattedFirstDay = format(firstDayOfWeek, 'MM/dd/yyyy', { locale: es });
    const auxformattedLastDay = format(lastDayOfWeek, 'MM/dd/yyyy', { locale: es });
    this.semanaCalcular = `${auxformattedFirstDay}  ${auxformattedLastDay}`;
    this.semana = `${formattedFirstDay}  ${formattedLastDay}`;
    return this.semana;
  }

  tarde(fecha: any) {
    const fechaHoy = new Date();
    const fechaParametro = new Date(fecha);
    if (fechaParametro < fechaHoy) {
      return true;
    } else {
      return false;
    }
  }

  formatoFecha(fecha: string): string {
    const fechaObj = new Date(fecha);
    const dia = fechaObj.getDate();
    const mes = fechaObj.getMonth() + 1;
    const año = fechaObj.getFullYear();
    return `${dia.toString().padStart(2, '0')}/${mes.toString().padStart(2, '0')}/${año}`;
  }

  async obtenerSesion() {
    const token: any = localStorage.getItem('token');
    const tokenPayload: any = jwt_decode(token);
    const resp: any = await this._service.obtenerUsuarioSesion(tokenPayload.emailUsuario).toPromise();
    this.idUsuario = resp[0].idUsuario;
    this.nombreUsuario = resp[0].nombreUsuario
    this.apellidoUsuario = resp[0].apellidoUsuario
    const hoy = new Date();
    const fechaHoy: Date = new Date(hoy.toISOString().split('T')[0]);
    this._service.obtenerActividadesUsuario(this.idUsuario).subscribe((resp: any) => {
      if (resp.length == 0) {
        this.aux = false
      } else {
        this.aux = true
        Object.keys(resp).forEach(nombreReunion => {
          const filteredReunion = resp[nombreReunion].filter((actividad: any) => {
            const fechaActividad: Date = new Date(actividad.dueDateToDo.split('T')[0]);
            const diferenciaEnDias = (fechaHoy.getTime() - fechaActividad.getTime()) / (1000 * 60 * 60 * 24);
            return !(diferenciaEnDias > 20 && actividad.statusToDo == 'Completado');
          });
          if (filteredReunion.length > 0) {
            this.actividades[nombreReunion] = filteredReunion;
          }
        });
      }
    }, err => {
      console.log(err);
    })
    this._service.obtenerIndicadoresUsuario(this.idUsuario).subscribe(async (resp: any) => {
      if (resp.length == 0) {
        this.aux1 = false
      } else {
        this.aux1 = true
        const clavesOrdenadas = Object.keys(resp).sort();

        // Crea un nuevo objeto indicadores con las claves ordenadas
        this.indicadores = {};
        clavesOrdenadas.forEach(clave => {
          this.indicadores[clave] = resp[clave];
        });
        let indicador: any = Object.values(this.indicadores)
        for (let index = 0; index < indicador.length; index++) {
          const aux = await this._reunion.obtenerSeguimiento(indicador[index][0].idMeasurable).toPromise();
          this.datos.push(aux);
        }
        for (let index = 0; index < this.datos.length; index++) {
          this.fechas[index] = [];
          for (let j = 0; j < this.datos[index].length; j++) {
            const fecha = this.datos[index][j].fechaMeasurable;
            if (!this.fechas[index].includes(fecha)) {
              this.fechas[index].push(fecha);
            }
          }
        }
        const compararFechas = (a: string, b: string) => {
          const fecha1 = new Date(a.split(' - ')[0].trim().split('/').reverse().join('/'));
          const fecha2 = new Date(b.split(' - ')[0].trim().split('/').reverse().join('/'));
          return fecha1.getTime() - fecha2.getTime();
        };
        for (let index = 0; index < this.fechas.length; index++) {
          this.fechas[index].sort(compararFechas);
          this.fechasFormateadas[index] = [];
          for (let j = 0; j < this.fechas[index].length; j++) {
            const aux = this.formatearFechas(this.fechas[index][j]);
            if (aux.length > 0) {
              this.fechasFormateadas[index].push(aux);
            }
          }
          this.fechasFormateadasReverso[index] = [...this.fechasFormateadas[index]].reverse();
          this.fechasReverso[index] = [...this.fechas[index]].reverse();
          if (this.fechas[index] && this.fechas[index].length > 0) {
            const ultimaFecha = this.fechas[index][this.fechas[index].length - 1].split(' - ')[0].trim();
            const ultimaFechaDate = new Date(ultimaFecha.split('/').reverse().join('/'));
            const fechaActual = new Date(this.semanaCalcular.split(' ')[0]);
            const semanasFaltantes: Date[] = [];
            for (let fecha = new Date(fechaActual); fecha > ultimaFechaDate; fecha.setDate(fecha.getDate() - 7)) {
              semanasFaltantes.push(new Date(fecha));
            }
            const semanasFaltantesTexto: string[] = semanasFaltantes.map(fecha => {
              const primeraFechaSemana = startOfWeek(fecha, { weekStartsOn: 1 });
              const ultimaFechaSemana = endOfWeek(fecha, { weekStartsOn: 1 });
              const formattedPrimeraFecha = format(primeraFechaSemana, 'dd/MM/yyyy', { locale: es });
              const formattedUltimaFecha = format(ultimaFechaSemana, 'dd/MM/yyyy', { locale: es });
              return `${formattedPrimeraFecha} - ${formattedUltimaFecha}`;
            });
            this.fechasRestantesTabla[index] = [];
            if (semanasFaltantesTexto.length > 0) {
              for (let k = 0; k < semanasFaltantesTexto.length; k++) {
                const aux = this.formatearFechas(semanasFaltantesTexto[k]);
                this.fechasRestantesTabla[index].push(aux);
              }
              const semanasFaltantesNoRepetidas = semanasFaltantesTexto.filter(semana => !this.fechas[index].includes(semana));
              this.fechasRestantes[index] = semanasFaltantesNoRepetidas;
            } else {
              this.fechasRestantes[index] = [];
            }
          } else {
            this.fechasRestantesTabla[index] = [];
            this.fechasRestantes[index] = [];
          }
        }
        this.cumplimiento()
      }
    }, err => {
      console.log(err);
    })


  }

  formatearFechas(intervalo: any) {    
    const aux = intervalo.split(' - ');
    const fechaInicio = parse(aux[0], 'dd/MM/yyyy', new Date());
    const fechaFin = parse(aux[1], 'dd/MM/yyyy', new Date());
    const formattedFirstDay = format(fechaInicio, 'MMM d', { locale: es });
    const formattedLastDay = format(fechaFin, 'MMM d', { locale: es });
    return `${formattedFirstDay}  ${formattedLastDay}`;
  }

  criterio(tipo: string) {
    if (tipo == 'Mayor que') {
      return '>'
    } else if (tipo == 'Mayor o igual que') {
      return '≥'
    } else if (tipo == 'Igual a') {
      return '='
    } else if (tipo == 'Menor o igual que') {
      return '≤'
    } else {
      return '<'
    }
  }

  meta(tipo: any) {
    if (tipo.unidadesMeasurable == 'Yenes') {
      return `¥${tipo.goalMetric}`
    } else if (tipo.unidadesMeasurable == 'Dolares') {
      return `${tipo.goalMetric}USD`
    } else if (tipo.unidadesMeasurable == 'Porcentaje') {
      return `${tipo.goalMetric}%`
    } else if (tipo.unidadesMeasurable == 'Libras') {
      return `£${tipo.goalMetric}`
    } else if (tipo.unidadesMeasurable == 'Pesos') {
      return `$${tipo.goalMetric}`
    } else {
      return tipo.goalMetric
    }
  }

  async cumplimiento(): Promise<void> {
    for (const [reunionKey, indicadoresArray] of Object.entries(this.indicadores)) {
      for (const indicador of indicadoresArray) {
        const idMeasurable = indicador.idMeasurable;
        for (let index = 0; index < this.fechasReverso.length; index++) {
          for (const fecha of this.fechasReverso[index]) {
            // Reemplazar diagonales ("/") por guiones ("-") en la fecha
            const intervalo = fecha.replace(/\//g, '-');
            //console.log("Intervalo", intervalo);
  
            this.data = { idMeasurable, fechaMeasurable: intervalo };
            this.resultado = await this._reunion.obtenerSeguimientoFechas(this.data).toPromise();
            this.resultados[reunionKey] = this.resultados[reunionKey] || {};
            this.resultados[reunionKey][idMeasurable] = this.resultados[reunionKey][idMeasurable] || [];
            const valor = this.resultado.length === 0 ? null : this.resultado[0].cumplimientoMeasurable.toString();
            this.resultados[reunionKey][idMeasurable].push(valor);
          }
        }
      }
    }
  }
  

  guardarSeguimiento(idMeasurable: any, cumplimiento: any, index: any) {
    // this.seguimiento.idMeasurable = idMeasurable
    // this.seguimiento.cumplimientoMeasurable = cumplimiento
    // this.seguimiento.fechaMeasurable = this.fechasReverso[index]
    // this._reunion.crearSeguimientoIndicador(this.seguimiento).subscribe((resp: any) => {
    //   console.log(resp);
    //   this.obtenerIndicadores()
    // }, err => {
    //   console.log(err);
    // })
  }

  guardarSeguimientoRestante(idMeasurable: any, cumplimiento: any, index: any) {
    // this.seguimiento.idMeasurable = idMeasurable
    // this.seguimiento.cumplimientoMeasurable = cumplimiento
    // this.seguimiento.fechaMeasurable = this.fechasRestantes[index]
    // this._reunion.crearSeguimientoIndicador(this.seguimiento).subscribe((resp: any) => {
    //   console.log(resp);
    //   this.obtenerIndicadores()
    // }, err => {
    //   console.log(err);
    // })
  }

  async completado(data: any) {
    try {
      if (data.statusToDo === 'Activo') {
        data.statusToDo = 'Completado';
        await this._reunion.actualizarStatusActividad(data).toPromise();
      } else if (data.statusToDo === 'Completado') {
        data.statusToDo = 'Activo';
        await this._reunion.actualizarStatusActividad(data).toPromise();
      }
    } catch (error) {
      console.error('Error al completar la actividad:', error);
    }
  }

}
