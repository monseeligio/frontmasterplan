import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';

@Component({
  selector: 'app-reunion',
  templateUrl: './reunion.component.html',
  styleUrls: ['./reunion.component.css']
})
export class ReunionComponent {

  paginas: any
  idReunion: any
  participantes: any
  respDatosReunion: any

  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.obtenerDetalles()
  }

  async obtenerDetalles() {
    this.route.params.subscribe(params => {
      const idReunionString = params['idReunion'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunion = 'R' + idNumero
    });
    this.paginas = await this._reunion.obtenerPaginas(this.idReunion).toPromise();
    this.obtenerReuniones()
  }

  async obtenerReuniones() {
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          nombre: datos.nombreUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario,
          pfp: datos.imagenUsuario
        };
      });
    } catch (error) {
      console.log(error);
    }
  }


  async iniciarReunion() {
    let data = {idReunion: this.idReunion, statusReunion: 'Iniciada'}
    await this._reunion.actualizarStatusReunion(data).toPromise()
    this.paginas = await this._reunion.obtenerPaginas(this.idReunion).toPromise();
    let auxPaginas = []
    for (let index = 0; index < this.paginas.length; index++) {
      auxPaginas.push(this.paginas[index].duracionPagina)
    }
    let dataIniciar = { fechaReunionIniciada: new Date(), idReunion: this.idReunion, minutoReunionIniciada: 0, minutosPaginas: auxPaginas.toString()}
    console.log("dataIniciar",dataIniciar);
    
    let idReunionIniciada: any = await this._reunion.nuevaReunionIniciada(dataIniciar).toPromise();

    this._router.navigate(['/reunionIniciada', idReunionIniciada.idReunionIniciada]);
  }



  async editarReunion() {
    this._router.navigate(['/editarReunion', this.idReunion]);
}

}
