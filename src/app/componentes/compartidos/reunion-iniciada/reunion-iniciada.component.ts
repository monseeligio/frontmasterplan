import { Component, TemplateRef, ViewChild, ElementRef, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { IndicadoresComponent } from '../paginas/indicadores/indicadores.component';
import Swal from 'sweetalert2';
import * as ClassicEditor from 'ckeditor5-custom/build/ckeditor';
import { MetasComponent } from '../paginas/metas/metas.component';
import { AsuntosComponent } from '../paginas/asuntos/asuntos.component';
import { ActividadesComponent } from '../paginas/actividades/actividades.component';
import { ProblemasComponent } from '../paginas/problemas/problemas.component';
import { Observable, interval } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConclusionComponent } from '../paginas/conclusion/conclusion.component';




@Component({
  selector: 'app-reunion-iniciada',
  templateUrl: './reunion-iniciada.component.html',
  styleUrls: ['./reunion-iniciada.component.css']
})
export class ReunionIniciadaComponent implements OnInit {
  @ViewChild(IndicadoresComponent) indicadorComp: IndicadoresComponent | undefined;
  @ViewChild(MetasComponent) metasComp: MetasComponent | undefined;
  @ViewChild(AsuntosComponent) asuntosComp: AsuntosComponent | undefined;
  @ViewChild(ActividadesComponent) actividadesComp: ActividadesComponent | undefined;
  @ViewChild(ProblemasComponent) problemasComp: ProblemasComponent | undefined;
  @ViewChild(ConclusionComponent) conclusionComp: ConclusionComponent | undefined;
  @ViewChild('agregarIndicadorModal', { static: false }) agregarIndicadorModal!: TemplateRef<any>;
  @ViewChild('agregarMetaModal', { static: false }) agregarMetaModal!: TemplateRef<any>;
  @ViewChild('agregarAsuntoModal', { static: false }) agregarAsuntoModal!: TemplateRef<any>;
  @ViewChild('crearProblemaModal', { static: false }) agregarProblemaModal!: TemplateRef<any>;
  @ViewChild('crearActividadModal', { static: false }) agregarActividadModal!: TemplateRef<any>;
  @ViewChild('inputElement') inputElement?: ElementRef;

  tinymce: any
  tokenPayload: any;
  reuniones: any;
  reunionesUsuario: any = []
  idUsuario: any;
  paginas: any;
  idReunion: any;
  idReunionIniciada: any
  participantes: any[] = [];
  respDatosReunion: any;
  minutos = 0;
  minutosPaginas: number[] = [];
  segundos = 0;
  horaLocal$!: Observable<string>;
  temporizador: any;
  paginaSeleccionada: any;
  iniciado: any;
  datosReunion: any;
  tipoPagina = 'rompehielo';
  editandoTitulo = false;
  nuevoTitulo = '';
  asuntos: any


  indicador = { idMeasurable: '', nombreMeasurable: '', unidadesMeasurable: '', tipoMedida: '', goalMetric: '', statusMeasurable: '', idUsuario: '', idReunion: '' };
  meta = { idGoal: '', tituloGoal: '', statusGoal: '', realGoal: '', acumuladoGoal: '', porcentajeGoal: 0, idUsuario: '', idReunion: '' };
  auxReal: any = false
  auxAcumulado: any = false
  asunto = { idHeadline: '', nombreHeadline: '', detallesHeadline: '', statusHeadline: '', idUsuario: '', idReunion: '', idReunionIniciada: '' };
  problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '', idReunionIniciada: '' }
  actividad: any = { idToDo: '', tituloToDo: '', detallesToDo: '', fechaCreadoToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }


  public Editor: any = ClassicEditor;


  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute, private _modal: NgbModal, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.obtenerDetalles().then(() => {
      this.horaLocal$ = interval(1000).pipe(
        map(() => this.getHoraLocal())
      );
    });

  }

  iniciarTemporizador() {
    setInterval(() => {
      this.minutos += 1;
      let data = { minutoReunionIniciada: this.minutos, idReunionIniciada: this.idReunionIniciada }
      this._reunion.actualizarMinutoReunionIniciada(data).toPromise()
    }, 60000);
  }

  getHoraLocal() {
    const now = new Date();
    const hora = now.getHours().toString().padStart(2, '0');
    const minutos = now.getMinutes().toString().padStart(2, '0');
    const segundos = now.getSeconds().toString().padStart(2, '0');
    return `${hora}:${minutos}:${segundos}`;
  }

  seleccionarPagina(index: number) {
    for (let index = 0; index < this.paginas.length; index++) {
      this.paginas[index].seleccionado = 'No'
    }
    this.paginas[index].seleccionado = "Si"
    this.paginaSeleccionada = this.paginas[index];
    this.tipoPagina = this.paginaSeleccionada.tipoPagina;
    if (this.temporizador) {
      clearInterval(this.temporizador);
    }
    this.actualizarTemporizador(index);
  }

  async actualizarTemporizador(index: number) {
    this.temporizador = setInterval(async () => {
      if (this.minutosPaginas[index] !== 0 || this.segundos !== 0) {
        if (this.segundos === 0) {
          if (this.minutosPaginas[index] > 0) {
            this.segundos = 59;
            this.minutosPaginas[index]--;
          } else if (this.minutosPaginas[index] === 0) {
            this.segundos = -1;
          }
        } else {
          this.segundos--;
        }
      } else {
        this.iniciado = false;
        clearInterval(this.temporizador);
      }
      let data = { minutosPaginas: this.minutosPaginas.toString(), idReunionIniciada: this.idReunionIniciada }
      await this._reunion.actualizarMinutosPaginas(data).toPromise()
    }, 1000);
  }

  async obtenerDetalles() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise()
      this.idReunion = auxiliarID[0].idReunion
      let statusAsunto = { idReunion: this.idReunion, idReunionIniciada: this.idReunionIniciada }
      this._reunion.actualizarStatusAsuntoNull(statusAsunto).toPromise()
      let statusActividad = { idReunion: this.idReunion, idReunionIniciada: this.idReunionIniciada }
      this._reunion.actualizarStatusActividadNull(statusActividad).toPromise()
      let statusProblema = { idReunion: this.idReunion, idReunionIniciada: this.idReunionIniciada }
      this._reunion.actualizarStatusProblemaNull(statusProblema).toPromise()
      this.minutos = auxiliarID[0].minutoReunionIniciada
      this.paginas = await this._reunion.obtenerPaginas(this.idReunion).toPromise();
      for (let index = 0; index < this.paginas.length; index++) {
        this.paginas[index].seleccionado = 'No'
        this.minutosPaginas.push(auxiliarID[0].minutosPaginas.split(',')[index])
      }
      this.iniciarTemporizador();
      this.seleccionarPagina(0);
      this.obtenerReuniones();
    });
  }

  async obtenerReuniones() {
    this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          idUsuario: datos.idUsuario,
          pfp: datos.imagenUsuario,
          nombre: datos.nombreUsuario,
          apellidos: datos.apellidoUsuario,
          email: datos.emailUsuario
        };
      });
    } catch (error) {
      console.log(error);
    }
  }

  abrirNuevoIndicadorModal() {
    this._modal.open(this.agregarIndicadorModal);
  }

  async agregarIndicador() {
    this.indicador.statusMeasurable = 'Activo'
    this.indicador.idReunion = this.idReunion
    if (this.indicador.goalMetric == "" || this.indicador.idUsuario == "" || this.indicador.nombreMeasurable == "" || this.indicador.tipoMedida == "" || this.indicador.unidadesMeasurable == "") {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this._reunion.crearIndicador(this.indicador).subscribe((resp: any) => {
        if (resp.mensaje == 'Registrado correctamente') {
          Swal.fire({
            icon: 'success',
            title: '¡Indicador creado exitosamente!',
            showConfirmButton: false,
            timer: 1500
          });
          this.cerrarModal()
          this.indicadorComp?.obtenerIndicadores()
          this.indicador = {
            idMeasurable: '',
            nombreMeasurable: '',
            unidadesMeasurable: '',
            tipoMedida: '',
            goalMetric: '',
            statusMeasurable: '',
            idUsuario: '',
            idReunion: ''
          };
        }
      }, (error) => {
        Swal.fire({
          icon: 'error',
          title: '¡Error!',
          text: 'Ocurrió un error.',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar'
        });
      })
    }
  }

  abrirNuevaMetaModal() {
    this._modal.open(this.agregarMetaModal);
  }

  agregarMeta() {
    if (this.meta.tituloGoal == '' || this.meta.idUsuario == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos!',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.meta.idReunion = this.idReunion
      this.meta.porcentajeGoal = 0
      this.meta.statusGoal = 'Activo'
      if (this.auxAcumulado === false) {
        this.meta.acumuladoGoal = 'No';
      } else if (this.auxAcumulado === true) {
        this.meta.acumuladoGoal = 'Si';
      }
      if (this.auxReal === false) {
        this.meta.realGoal = 'No';
      } else if (this.auxReal === true) {
        this.meta.realGoal = 'Si';
      }
      this._reunion.crearMeta(this.meta).subscribe((resp: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Meta creada exitosamente!',
          showConfirmButton: false,
          timer: 1500
        });
        this.meta = { idGoal: '', tituloGoal: '', statusGoal: '', realGoal: '', acumuladoGoal: '', porcentajeGoal: 0, idUsuario: '', idReunion: '' };
        this.metasComp?.obtenerMetas()
        this.cerrarModal();
      }, (error) => {
        console.log(error);
      })
    }
  }

  abrirNuevoAsuntoModal() {
    this._modal.open(this.agregarAsuntoModal);
  }

  agregarAsunto() {
    if (this.asunto.nombreHeadline == '' || this.asunto.idUsuario == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.asunto.idReunion = this.idReunion
      this.asunto.idReunionIniciada = this.idReunionIniciada
      this.asunto.statusHeadline = 'Activo'
      this._reunion.crearAsunto(this.asunto).subscribe((resp: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Asunto creado con éxito!',
          showConfirmButton: false,
          timer: 1500
        });
        this.asunto = {
          idHeadline: '', nombreHeadline: '', detallesHeadline: '', statusHeadline: '', idUsuario: '', idReunion: '', idReunionIniciada: ''
        };
        this.asuntosComp?.obtenerAsuntos()
        this.cerrarModal();
      }, (error) => {
        console.log(error);
      })
    }
  }

  abrirNuevaActividadModal() {
    this._modal.open(this.agregarActividadModal);
  }

  async agregarActividad() {
    if (this.actividad.tituloToDo == '' || this.actividad.dueDateToDo == '' || this.actividad.idUsuario.length < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.actividad.idReunion = this.idReunion
      this.actividad.statusToDo = 'Activo'
      this.actividad.idReunionIniciada = this.idReunionIniciada
      for (let index = 0; index < this.actividad.idUsuario.length; index++) {
        const data = { tituloToDo: this.actividad.tituloToDo, detallesToDo: this.actividad.detallesToDo, dueDateToDo: this.actividad.dueDateToDo, statusToDo: this.actividad.statusToDo, idUsuario: this.actividad.idUsuario[index], idReunion: this.idReunion, idReunionIniciada: this.idReunionIniciada }
        await this._reunion.crearActividad(data).toPromise()
      }
      this.actividad = { idToDo: '', tituloToDo: '', detallesToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '', idReunionIniciada: '' }
      Swal.fire({
        icon: 'success',
        title: 'Se creó la actividad con éxito',
        showConfirmButton: false,
        timer: 1500
      });
      this.actividadesComp?.obtenerActividades()
      this.conclusionComp?.obtenerActividades()
      this.cerrarModal()
    }
  }

  abrirNuevoProblemaModal() {
    this._modal.open(this.agregarProblemaModal);
  }

  agregarProblema() {
    if (this.problema.nombreIssue == '' || this.problema.idUsuario == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.problema.idReunion = this.idReunion
      this.problema.statusIssue = 'Activo'
      let fechaHoy = new Date();
      let fechaSolo = fechaHoy.toISOString().split('T')[0];
      this.problema.fechaIssue = fechaSolo
      this.problema.idReunionIniciada = this.idReunionIniciada
      this._reunion.crearProblema(this.problema).subscribe((resp: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Problema creado exitosamente!',
          showConfirmButton: false,
          timer: 1500
        });
        this.problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '', idReunionIniciada: '' }
        this.problemasComp?.obtenerProblemas()
        this.cerrarModal()
      }, err => {
        console.log(err);
      })
    }
  }


  cerrarModal() {
    this._modal.dismissAll();
  }


  activarEdicionTituloMetas() {
    this.editandoTitulo = true;
    this.nuevoTitulo = this.metasComp?.metas[this.metasComp?.index].tituloGoal;
    if (this.inputElement) {
      this.inputElement.nativeElement.focus();
    }
  }

  activarEdicionTituloAsuntos() {
    this.nuevoTitulo = ''
    this.editandoTitulo = true;
    this.nuevoTitulo = this.asuntosComp?.asuntos[this.asuntosComp?.index].nombreHeadline;
  }

  guardarNuevoTituloAsunto() {
    let data = { nombreHeadline: this.nuevoTitulo, idHeadline: this.asuntosComp?.asuntos[this.asuntosComp?.index].idHeadline }
    this._reunion.actualizarTituloAsunto(data).toPromise()
    this.asuntosComp?.obtenerAsuntos()
    this.editandoTitulo = false;
  }

  activarEdicionTituloActividades() {
    this.nuevoTitulo = ''
    this.editandoTitulo = true;
    this.nuevoTitulo = this.actividadesComp?.actividades[this.actividadesComp?.index].tituloToDo;
  }

  guardarNuevoTituloActividad() {
    let data = { tituloToDo: this.nuevoTitulo, idToDo: this.actividadesComp?.actividades[this.actividadesComp?.index].idToDo }
    this._reunion.actualizarTituloActividad(data).toPromise()
    this.actividadesComp?.obtenerActividades()
    this.editandoTitulo = false;
  }

  activarEdicionTituloProblemas() {
    this.nuevoTitulo = ''
    this.editandoTitulo = true;
    this.nuevoTitulo = this.problemasComp?.problemas[this.problemasComp?.index].nombreIssue;
  }

  guardarNuevoTituloProblemas() {
    let data = { nombreIssue: this.nuevoTitulo, idIssue: this.problemasComp?.problemas[this.problemasComp?.index].idIssue }
    this._reunion.actualizarTituloProblema(data).toPromise()
    this.problemasComp?.obtenerProblemas()
    this.editandoTitulo = false;
  }

  guardarNuevoTitulo() {
    console.log(this.nuevoTitulo);
    this.editandoTitulo = false;
  }

  cancelarEdicionTitulo() {
    this.editandoTitulo = false;
  }



  actualizarUsuarioH(data: any) {
    this.asuntosComp?.actualizarUsuario(data)
  }

  actualizarUsuarioI(data: any) {
    this.problemasComp?.actualizarUsuario(data)
  }

  actualizarFechaA(data: any) {
    this.actividadesComp?.actualizarFecha(data)
  }

  actualizarUsuarioA(data: any) {
    this.actividadesComp?.actualizarUsuario(data)
  }
}
