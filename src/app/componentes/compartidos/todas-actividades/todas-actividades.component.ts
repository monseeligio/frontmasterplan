import { Component, OnInit, ChangeDetectorRef, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { format, startOfWeek, endOfWeek, parseISO, parse } from 'date-fns';
import { es } from 'date-fns/locale';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { forkJoin } from 'rxjs';

import jwt_decode from "jwt-decode";
import { ProyectosService } from 'src/app/servicios/proyectos.service';

@Component({
  selector: 'app-todas-actividades',
  templateUrl: './todas-actividades.component.html',
  styleUrls: ['./todas-actividades.component.css']
})
export class TodasActividadesComponent implements OnInit {

  @ViewChild('crearRocaModal', { static: false }) crearRocaModal!: TemplateRef<any>;
  @ViewChild('crearActividadModal', { static: false }) crearActividadModal!: TemplateRef<any>;
  @ViewChild('editarRocaModal', { static: false }) editarRocaModal!: TemplateRef<any>;
  @ViewChild('editarActividadModal', { static: false }) editarActividadModal!: TemplateRef<any>;
  @ViewChild('planearActividadModal', { static: false }) planearActividadModal!: TemplateRef<any>;

  diaSeleccionado: any
  mesSeleccionado: any;
  anioSeleccionado: any
  idUsuario: any
  rocas: any = []
  actividades: any = []
  porcentaje: any = []
  completadas: any = []
  nombre: any
  actividadesSecundarias: any = []
  planeadas: any = []
  act: any
  tokenPayload: any;
  hoy: any
  aux: any
  auxNombre: any
  diasMes: any = []
  status: any = []
  fechas: any = []
  statAct = false
  completado = false
  fechaActividad: any
  colaboradores: any
  roca = { nombreRoca: '', statusRoca: '', idUsuario: '' }
  actividad = { nombreActividad: '', statusActividad: '', idRoca: '', idUsuario: null }
  constructor(private _proyecto: ProyectosService, private _service: ApirestService, private _router: Router, private route: ActivatedRoute, private _modal: NgbModal, private cdr: ChangeDetectorRef) { }

  async ngOnInit() {
    const token: any = localStorage.getItem('token')
    this.tokenPayload = jwt_decode(token);
    const idUsuario: any = await this._service.obtenerIDUsuario(this.tokenPayload.emailUsuario).toPromise();
    this.idUsuario = idUsuario[0].idUsuario;
    this.hoy = new Date()
    this.mesSeleccionado = this.hoy.getMonth()
    this.anioSeleccionado = this.hoy.getFullYear()
    this.obtenerDias(this.mesSeleccionado, this.anioSeleccionado)
    this.obtenerRocas(this.idUsuario)
    this.todos()

  }

  todos() {
    if (this.idUsuario === 'U41' || this.idUsuario === 'U42') {
      this._service.obtenerUsuarios().subscribe((resp: any) => {
        this.colaboradores = resp;
        this.colaboradores = this.colaboradores.filter((usuario: any) => usuario.statusUsuario === 'Activo');
        function compare(a: any, b: any): number {
          return a.idUsuario.split("U")[1] - b.idUsuario.split("U")[1];
        }
        this.colaboradores.sort(compare);
        for (const participantes of this.colaboradores) {
          if (participantes.idUsuario !== this.idUsuario) {
            this.resetearDatos(participantes.idUsuario)
            this.obtenerRocas(participantes.idUsuario);
          }
        }
      });
    } else {
      this._proyecto.obtenerColaboradores(this.idUsuario).subscribe((resp: any) => {
        this.colaboradores = resp;
        this.colaboradores = this.colaboradores.filter((usuario: any) => usuario.statusUsuario === 'Activo');
        function compare(a: any, b: any): number {
          return a.idUsuario.split("U")[1] - b.idUsuario.split("U")[1];
        }
        this.colaboradores.sort(compare);
        if (this.colaboradores.length > 0) {
          for (const participantes of this.colaboradores) {
            if (participantes.idUsuario !== this.idUsuario) {
              this.resetearDatos(participantes.idUsuario)
              this.obtenerRocas(participantes.idUsuario);
            }
          }
        }

      });
    }
  }


  obtenerRocas(idUsuario: string): void {
    this.resetearDatos(idUsuario);
    this._proyecto.obtenerRocas(idUsuario).subscribe(
      (rocasResp: any) => {
        this.rocas[idUsuario] = rocasResp.filter((roca: { statusRoca: string }) => roca.statusRoca === 'Activa');
        function compare(a: any, b: any): number {
          return a.idRoca.split("R")[1] - b.idRoca.split("R")[1];
        }
        this.rocas[idUsuario].sort(compare);
        const observables = this.rocas[idUsuario].map((roca: { idRoca: any }) => this._proyecto.obtenerActividades(roca.idRoca));
        forkJoin(observables).subscribe(
          (respuestas: any) => {
            respuestas.forEach((resp: any, i: number) => {
              const roca = this.rocas[idUsuario][i];
              if (Array.isArray(resp) && resp.length > 0) {
                const filteredActividades = resp.filter((actividad: { statusActividad: string }) => actividad.statusActividad !== 'Archivado');
                this.agregarActividades(idUsuario, roca.idRoca, filteredActividades);
              }
            });
            this._proyecto.obtenerActividadesSecundarias(idUsuario).subscribe((actividadesSecundariasResp: any) => {
              
              const filteredActividadesSecundarias = actividadesSecundariasResp.filter((actividad: { statusActividad: string }) => actividad.statusActividad !== 'Archivado');
              this.actividadesSecundarias[idUsuario] = filteredActividadesSecundarias;
              //console.log(this.actividadesSecundarias[idUsuario]);
              function compare(a: any, b: any): number {
                return a.idActividad.split("A")[1] - b.idActividad.split("A")[1];
              }
              this.actividadesSecundarias[idUsuario].sort(compare);
              this.obtenerStatusDia(idUsuario);
            });
          },
          (err) => {
            console.error(`Error en la suscripción a obtenerActividades: ${err.message}`);
          }
        );
      },
      (err) => {
        console.error(`Error en la suscripción a obtenerRocas: ${err.message}`);
      }
    );
  }

  resetearDatos(idUsuario: string): void {
    this.rocas[idUsuario] = [];
    this.actividades[idUsuario] = [];
    this.actividadesSecundarias[idUsuario] = [];
    this.status = []
    this.status[idUsuario] = [];
    this.status[idUsuario]['Sin roca'] = [];
  }


  agregarActividades(idUsuario: string, idRoca: string, actividades: any): void {
    if (!this.actividades[idUsuario][idRoca]) {
      this.actividades[idUsuario][idRoca] = [];
    }
    this.actividades[idUsuario][idRoca].push(actividades);
    function compare(a: any, b: any): number {
      return a.idActividad.split("A")[1] - b.idActividad.split("A")[1];
    }
    this.actividades[idUsuario][idRoca][0].sort(compare);
  }



  obtenerDias(mes: number, anio: number) {
    this.diasMes = [];
    const cantidadDias = new Date(anio, mes + 1, 0).getDate();
    for (let i = 1; i <= cantidadDias; i++) {
      this.diasMes.push(i);
    }
  }

  seleccionarMes() {
    this.obtenerDias(+this.mesSeleccionado, +this.anioSeleccionado)
    for (const participantes of this.colaboradores) {
      this.obtenerStatusDia(participantes.idUsuario)
    }
  }

  obtenerStatusDia(idUsuario: any) {
    this.status[idUsuario] = [];
    this.completadas[idUsuario] = [];
    this.planeadas[idUsuario] = [];
    this.porcentaje[idUsuario] = [];
    this.mesSeleccionado++;
    this.fechas = this.diasMes.map((dia: { toString: () => string; }) => {
      const formattedDia = dia.toString().padStart(2, '0');
      return `${formattedDia}-${this.mesSeleccionado.toString().padStart(2, '0')}-2024`;
    });
    for (const roca of this.rocas[idUsuario]) {
      for (const actividadGroup of this.actividades[idUsuario][roca.idRoca] || []) {
        for (const actividad of actividadGroup || []) {
          this.status[idUsuario][actividad.idActividad] = [];
          let auxPlaneadas = 0;
          let auxCompletadas = 0;
          const observables = this.fechas.map((fecha: string | number | boolean) =>
            this._proyecto.obtenerStatus(actividad.idActividad, fecha)
          );
          forkJoin(observables).subscribe(
            (respArray: any) => {
              respArray.forEach((resp: string | any[]) => {
                if (resp.length === 0 || resp[0].statusActividad === '') {
                  this.status[idUsuario][actividad.idActividad] = this.status[idUsuario][actividad.idActividad] || [];
                  this.status[idUsuario][actividad.idActividad].push(null);
                } else {
                  this.status[idUsuario][actividad.idActividad] = this.status[idUsuario][actividad.idActividad] || [];
                  this.status[idUsuario][actividad.idActividad].push(resp[0].statusActividad);
                  if (resp[0].statusActividad === 'Completado' || resp[0].statusActividad === 'Planeado') {
                    auxPlaneadas++;
                  }
                  if (resp[0].statusActividad === 'Completado') {
                    auxCompletadas++;
                  }
                }
              });
              this.completadas[idUsuario][actividad.idActividad] = auxCompletadas || null;
              this.planeadas[idUsuario][actividad.idActividad] = auxPlaneadas || null;
              if (auxPlaneadas !== 0) {
                this.porcentaje[idUsuario][actividad.idActividad] = Math.floor((auxCompletadas * 100) / auxPlaneadas) || 0;
              } else {
                this.porcentaje[idUsuario][actividad.idActividad] = null;
              }
            },
            (error) => {
              console.error(`Error obteniendo status para la actividad ${actividad.idActividad}:`, error);
            }
          );
        }
      }
    }
    this.status[idUsuario] = this.status[idUsuario] || [];
    this.status[idUsuario]['Sin roca'] = this.status[idUsuario]['Sin roca'] || [];
    for (const actividadSec of this.actividadesSecundarias[idUsuario]) {
      let auxPlaneadas = 0;
      let auxCompletadas = 0;
      for (const fecha of this.fechas) {
        this._proyecto.obtenerStatus(actividadSec.idActividad, fecha).subscribe(
          (resp: any) => {
            const statusActividad = (resp.length > 0 && resp[0].statusActividad) || null;
            this.status[idUsuario]['Sin roca'][actividadSec.idActividad] = this.status[idUsuario]['Sin roca'][actividadSec.idActividad] || [];
            this.status[idUsuario]['Sin roca'][actividadSec.idActividad].push(statusActividad);
            if (statusActividad === 'Completado' || statusActividad === 'Planeado') {
              auxPlaneadas++;
            }
            if (statusActividad === 'Completado') {
              auxCompletadas++;
            }
            this.completadas[idUsuario][actividadSec.idActividad] = auxCompletadas || null;
            this.planeadas[idUsuario][actividadSec.idActividad] = auxPlaneadas || null;
            if (auxPlaneadas !== 0) {
              this.porcentaje[idUsuario][actividadSec.idActividad] = Math.floor((auxCompletadas * 100) / auxPlaneadas) || 0;
            } else {
              this.porcentaje[idUsuario][actividadSec.idActividad] = null;
            }
          },
          (error) => {
            console.error(`Error obteniendo status para la actividad ${actividadSec.idActividad} en la fecha ${fecha}:`, error);
          }
        );
      }
    }
    this.mesSeleccionado--
  }

  abrirNuevaRocaModal() {
    this._modal.open(this.crearRocaModal);
  }

  async crearRoca() {
    try {
      if (this.roca.nombreRoca == '') {
        Swal.fire({
          icon: 'error',
          title: '¡Error!',
          text: 'Llene todos los campos!',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar'
        });
      } else {
        this.roca.statusRoca = 'Activa';
        this.roca.idUsuario = this.idUsuario;
        await this._proyecto.nuevaRoca(this.roca).toPromise();
        this.roca.nombreRoca = '';
        this.obtenerRocas(this.idUsuario);
        this.todos()
        this.cerrarModal();

      }
    } catch (error) {
      console.error('Error al crear la roca:', error);
      // Aquí puedes agregar un mensaje de error o alguna acción adicional si es necesario
    }
  }


  abrirEditarRocaModal(roca: any) {
    this.auxNombre = roca.nombreRoca
    this.aux = roca.idRoca
    this._modal.open(this.editarRocaModal);
  }

  async editarRoca() {
    if (this.auxNombre == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos!',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      let data = { nombreRoca: this.auxNombre, idRoca: this.aux }
      await this._proyecto.editarRoca(data).toPromise()
      this.roca.nombreRoca = ''
      this.obtenerRocas(this.idUsuario)
      this.todos()

      this.cerrarModal()
    }
  }

  async archivarRoca() {
    const confirmed = await Swal.fire({
      title: '¿Seguro que desea archivar esta roca?',
      text: '¡No podrás revertir este cambio!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Archivar'
    }).then((result) => result.isConfirmed);
    if (confirmed) {
      const data = { idRoca: this.aux };
      try {
        await this._proyecto.archivarRoca(data).toPromise();
        this.cerrarModal();
        this.obtenerRocas(this.idUsuario);
        this.todos()

        this.roca = { nombreRoca: '', statusRoca: '', idUsuario: '' };
      } catch (error) {
        console.error('Error al archivar la roca:', error);
      }
    }
  }


  abrirNuevaActividadModal() {
    this._modal.open(this.crearActividadModal);
  }

  async crearActividad() {
    if (this.actividad.nombreActividad == '' || this.actividad.idRoca == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos!',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      if (this.actividad.idRoca == 'Sin roca') {
        let data = { nombreActividad: this.actividad.nombreActividad, statusActividad: 'Activa', idRoca: null, idUsuario: this.idUsuario }        
        await this._proyecto.nuevaActividad(data).toPromise()
        this.obtenerRocas(this.idUsuario)
        this.todos()
        this.cerrarModal()
        this.actividad = { nombreActividad: '', statusActividad: '', idRoca: '', idUsuario: null }
      } else {
        this.actividad.statusActividad = 'Activa'
        await this._proyecto.nuevaActividad(this.actividad).toPromise()
        this.obtenerRocas(this.idUsuario)
        this.todos()
        this.cerrarModal()
        this.actividad = { nombreActividad: '', statusActividad: '', idRoca: '', idUsuario: null }
      }
    }
  }

  abrirEditarActividadModal(actividad: any) {
    this.actividad.nombreActividad = actividad.nombreActividad
    this.aux = actividad.idActividad
    this._modal.open(this.editarActividadModal);
  }

  async editarActividad() {
    if (this.actividad.nombreActividad == '') {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos!',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      let data = { nombreActividad: this.actividad.nombreActividad, idActividad: this.aux }
      await this._proyecto.editarActividad(data).toPromise()
      this.obtenerRocas(this.idUsuario)
      this.todos()

      this.cerrarModal()
      this.actividad = { nombreActividad: '', statusActividad: '', idRoca: '', idUsuario: null }
    }
  }

  async archivarActividad() {
    const confirmed = await Swal.fire({
      title: '¿Seguro que desea archivar esta actividad?',
      text: '¡No podrás revertir este cambio!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Archivar'
    }).then((result) => result.isConfirmed);
    if (confirmed) {
      const data = { idActividad: this.aux };
      try {
        await this._proyecto.archivarActividad(data).toPromise();
        this.cerrarModal();
        this.obtenerRocas(this.idUsuario);
        this.todos()

        this.actividad = { nombreActividad: '', statusActividad: '', idRoca: '', idUsuario: null };
      } catch (error) {
        console.error('Error al archivar la actividad:', error);
      }
    }
  }


  abrir(dia: any, status: any, Actividad: any) {
    this.diaSeleccionado = dia
    console.log(this.diaSeleccionado, status, Actividad);
    if (status == 'Planeado') {
      this.statAct = true
      this.completado = false
    } else if (status =='Completado') {
      this.statAct = true
      this.completado = true
    } else if (status == null) {
      this.statAct = false
    }
    this.act = Actividad.idActividad
    this.nombre = Actividad.nombreActividad
    this._modal.open(this.planearActividadModal)
  }


  async planear() {
    try {
      let data: { idActividad: string; statusActividad: string; fechaActividad: string } = { idActividad: '', statusActividad: '', fechaActividad: '' };
      if (this.statAct == false) {
        data = { idActividad: this.act, statusActividad: '', fechaActividad: this.diaSeleccionado };
      } else if (this.statAct ==true) {
        data = { idActividad: this.act, statusActividad: 'Planeado', fechaActividad: this.diaSeleccionado };
      }
      const resp: any = await this._proyecto.obtenerStatus(this.act, this.diaSeleccionado).toPromise();
      if (resp.length === 0) {
        await this._proyecto.nuevoPlan(data).toPromise();
        this.cerrarModal();
        this.obtenerRocas(this.idUsuario);
        this.todos()

      } else {
        await this._proyecto.actualizarStatusActividad(data).toPromise();
        this.statAct = false;
        this.completado = false;
        this.act = '';
        this.diaSeleccionado = '';
        data = { idActividad: '', statusActividad: '', fechaActividad: '' };
        this.cerrarModal();
        this.obtenerRocas(this.idUsuario);
        this.todos()
      }
    } catch (error) {
      console.error('Error en la operación:', error);
    }
  }


  async completar() {
    let data = { idActividad: '', statusActividad: '', fechaActividad: '' }
    if (this.completado == false) {
      data = { idActividad: this.act, statusActividad: 'Planeado', fechaActividad: this.diaSeleccionado };
    }
    if (this.completado == true) {
      data = { idActividad: this.act, statusActividad: 'Completado', fechaActividad: this.diaSeleccionado };
    }
    try {
      // const resp: any = await this._proyecto.obtenerStatus(this.act, encodeURIComponent(this.diaSeleccionado)).toPromise();
      // if (resp.length == 0) {
      //   await this._proyecto.nuevoPlan(data).toPromise();
      //   this.obtenerRocas();
      // } else {
        console.log(data);
        
      await this._proyecto.actualizarStatusActividad(data).toPromise();
      this.statAct = false
      this.completado = false
      this.act = ''
      this.diaSeleccionado = ''
      data = { idActividad: '', statusActividad: '', fechaActividad: '' }
      this.cerrarModal()
      this.obtenerRocas(this.idUsuario);
      this.todos()
    } 
    catch (error) {
      console.error('Error en la operación:', error);
    }

  }


  cerrarModal() {
    this._modal.dismissAll();
  }

}
