import { Component, ChangeDetectorRef, ViewChild, OnInit } from '@angular/core';
import { PanZoomConfig, PanZoomAPI } from 'ngx-panzoom';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { TreeNode } from 'primeng/api';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css'],
})
export class PersonasComponent implements OnInit {
  panZoomConfig: PanZoomConfig = new PanZoomConfig({
    zoomLevels: Infinity,
    neutralZoomLevel: 3,
    scalePerZoomLevel: 2, // Aumentado el factor de escala para un alejamiento más pronunciado
    initialZoomLevel: 3,
    zoomToFitZoomLevelFactor: 0.5, // Reducido el factor de zoom al ajustar a la vista
    initialPanX: 0,
    initialPanY: 0,
    freeMouseWheel: true,
    invertMouseWheel: true,
    zoomOnDoubleClick: true,
    zoomOnMouseWheel: true,
});




  datosUsuario: any;
  data: any
  treeData: any
  nodes: TreeNode[] = [];
  usuarioMap: any

  idSeleccionado: any
  titulo: any
  img: any
  usuario: any
  roles: any
  nuevoRol: string = '';
  supervisor: any
  personal: any
  mostrarInput: boolean = false;


  roleUsuario: String = ''
  emailUsuario: any

  constructor(private _service: ApirestService, private cdr: ChangeDetectorRef) { }

  async ngOnInit() {
    this.obtenerUsuario()
    this.obtenerSesion()
  }

  async obtenerSesion() {
    const token: any = localStorage.getItem('token');
    const tokenPayload: any = jwt_decode(token);
    this.roleUsuario = tokenPayload.roleUsuario;
    this.emailUsuario = tokenPayload.emailUsuario;

    const resp: any = await this._service.obtenerUsuarioSesion(tokenPayload.emailUsuario).toPromise();

    this.titulo = resp[0].puestoUsuario
    this.usuario = resp[0].nombreUsuario + ' ' + resp[0].apellidoUsuario;
    this.img = `http://149.56.248.111:5057/imgs/usuarios/` + resp[0].imagenUsuario;
    this.roles = await this._service.obtenerRolesUsuario(resp[0].idUsuario).toPromise()
  }


  async obtenerUsuario() {
    this.datosUsuario = await this._service.obtenerUsuarios().toPromise();
    this.datosUsuario = this.datosUsuario.filter((usuario: any) => usuario.statusUsuario === 'Activo');
    function compare(a: any, b: any): number {
      return a.idUsuario.split("U")[1] - b.idUsuario.split("U")[1];
    }
    this.datosUsuario.sort(compare);
    this.usuarioMap = new Map();
    this.datosUsuario.forEach((usuario: any) => {
      this.usuarioMap.set(usuario.idUsuario, {
        id: usuario.idUsuario,
        title: usuario.puestoUsuario,
        img: `http://149.56.248.111:5057/imgs/usuarios/` + usuario.imagenUsuario,
        label: usuario.nombreUsuario + ' ' + usuario.apellidoUsuario,
        type: 'person',
        expanded: true,
        children: []
      });
    });
    this.agregarHijos();
    this.cdr.detectChanges();
  }

  private agregarHijos() {
    this.usuarioMap.forEach((nodo: TreeNode, idUsuario: any) => {
      const jefeDirecto = this.datosUsuario.find((usuario: { idUsuario: any; }) => usuario.idUsuario === idUsuario).jefeDirectoUsuario;
      if (jefeDirecto && this.usuarioMap.has(jefeDirecto)) {
        const jefeNode = this.usuarioMap.get(jefeDirecto);
        jefeNode.children.push(nodo);
      } else {
        this.nodes.push(nodo);
      }
    });
  }

  async mostrarDetalles(detalles: any) {
    this.idSeleccionado = detalles.id
    this.titulo = detalles.title
    this.usuario = detalles.label;
    this.img = detalles.img;
    this.roles = await this._service.obtenerRolesUsuario(this.idSeleccionado).toPromise();

  }

  async agregarRol() {
    if (this.nuevoRol.trim() !== '') {
      let data = { tituloRole: this.nuevoRol, idUsuario: this.idSeleccionado }
      this._service.nuevoRoleUsuario(data).toPromise()
      this.roles = await this._service.obtenerRolesUsuario(this.idSeleccionado).toPromise()
      this.mostrarInput = false;
      this.nuevoRol = '';
    }
  }

  async eliminarRole(rol: any) {
    this._service.eliminarRoleUsuario(rol).toPromise()
    this.roles = await this._service.obtenerRolesUsuario(this.idSeleccionado).toPromise()
  }
}
