import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { HttpClient } from '@angular/common/http';
import jwt_decode from "jwt-decode";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css']
})
export class EditarPerfilComponent implements OnInit {

  datosUsuarioActualizar = {
    idUsuario: '', nombreUsuario: '', apellidoUsuario: ''
  };
  idUsuario: any
  nombreUsuario: String = ''
  apellidoUsuario: String = ''

  images: any;
  imgURL = '../../../../assets/img/NoImagen.png';

  constructor(private _service: ApirestService, private _router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.obtenerSesion()
  }

  async obtenerSesion() {
    const token: any = localStorage.getItem('token');
    const tokenPayload: any = jwt_decode(token);
    const resp: any = await this._service.obtenerUsuarioSesion(tokenPayload.emailUsuario).toPromise();
    this.idUsuario = resp[0].idUsuario;
    this.datosUsuarioActualizar.nombreUsuario = resp[0].nombreUsuario;
    this.datosUsuarioActualizar.apellidoUsuario = resp[0].apellidoUsuario;
    const aux = this.nombreUsuario.split(" ");
    if (aux.length > 1) {
      this.nombreUsuario = aux[0];
    }
    if (resp[0].imagenUsuario != 'No') {
      this.imgURL = 'http://149.56.248.111:5057/imgs/usuarios/' + resp[0].imagenUsuario
    }
  }

  selectedImage(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files && target.files.length > 0) {
      const file = target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (event: any) => {
        this.imgURL = event.target.result;
      }
      this.images = file;
    }
    this.subirImagen()
  }

  subirImagen() {
    const formData = new FormData();
    formData.append('imagen', this.images);
    console.log(formData.get('imagen'));

    this.http.patch(`http://149.56.248.111:5057/usuario/cambiarPFP/${this.idUsuario}`, formData).subscribe(
      (response) => {
        // console.log(response);
      }, (error) => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Hubo un error.',
        });
      });
  }

  actualizar() {
    this.datosUsuarioActualizar.idUsuario = this.idUsuario
    this.http.patch(`http://149.56.248.111:5057/usuario/actualizarNombreUsuario/`, this.datosUsuarioActualizar).subscribe(
      (response) => {
        // console.log(response);
      }, (error) => {
        console.log(error);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Hubo un error.',
        });
      });
  }
}
