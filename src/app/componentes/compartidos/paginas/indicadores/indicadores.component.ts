import { Component, OnInit, ChangeDetectorRef, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import jwt_decode from "jwt-decode";
import { format, startOfWeek, endOfWeek, subDays, parse } from 'date-fns';
import { es } from 'date-fns/locale';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-indicadores',
  templateUrl: './indicadores.component.html',
  styleUrls: ['./indicadores.component.css']
})
export class IndicadoresComponent implements OnInit {

  @ViewChild('editarIndicadorModal', { static: false }) editarIndicadorModal!: TemplateRef<any>;

  roleUsuario: any
  tokenPayload: any;
  semana: any;
  semanaPasada: any
  semanaCalcular: any
  semanaAux: any
  measurables: any;
  cumplimientos: any = [];
  fechas: any = [];
  fechasRestantes: any = []
  fechasRestantesTabla: any = []
  fechasReverso: any;
  fechasFormateadas: any = [];
  fechasFormateadasReverso: any = [];
  idReunion: any;
  idReunionIniciada: any
  participantes: any;
  respDatosReunion: any;
  datos: any = [];
  aux: any = [];
  valorTH: any;
  ayuda: any = [];
  data = { idMeasurable: '', fechaMeasurable: '' };
  resultado: any
  resultados: any = {}
  semanaAnterior: any
  seguimiento = { idMeasurable: String, cumplimientoMeasurable: '', fechaMeasurable: String }

  indicador = { idMeasurable: '', nombreMeasurable: '', tipoMedida: '', goalMetric: '', statusMeasurable: '', idUsuario: '', idReunion: '' };


  constructor(
    private _service: ApirestService,
    private _reunion: ReunionesService,
    private _router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private _modal: NgbModal
  ) { }

  ngOnInit(): void {
    this.token()
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise()
      this.idReunion = auxiliarID[0].idReunion
      this.obtenerReuniones();
      this.obtenerRangoSemana()
      this.obtenerIndicadores();
    });
  }

  token() {
    const token: any = localStorage.getItem('token')
    this.tokenPayload = jwt_decode(token);
    this.roleUsuario = this.tokenPayload.roleUsuario;
  }


  obtenerRangoSemana(): string {
    const today = new Date();
    const firstDayOfWeek = startOfWeek(today, { weekStartsOn: 1 });
    const lastDayOfWeek = endOfWeek(today, { weekStartsOn: 1 });
    const firstDayOfLastWeek = subDays(firstDayOfWeek, 7);
    const lastDayOfLastWeek = subDays(lastDayOfWeek, 7);
    const formattedFirstDay = format(firstDayOfWeek, 'MMM d', { locale: es });
    const formattedLastDay = format(lastDayOfWeek, 'MMM d', { locale: es });
    const aux = format(firstDayOfWeek, 'dd/MM/yyyy', { locale: es });
    const aux1 = format(lastDayOfWeek, 'dd/MM/yyyy', { locale: es });
    this.semanaAux = `${aux} - ${aux1}`;
    const formattedLastFirstDay = format(firstDayOfLastWeek, 'dd/MM/yyyy', { locale: es });
    const formattedLastLastDay = format(lastDayOfLastWeek, 'dd/MM/yyyy', { locale: es });
    const auxFormattedFirstDay = format(firstDayOfWeek, 'MM/dd/yyyy', { locale: es });
    const auxFormattedLastDay = format(lastDayOfWeek, 'MM/dd/yyyy', { locale: es });
    this.semanaCalcular = `${auxFormattedFirstDay}  ${auxFormattedLastDay}`;
    this.semanaPasada = `${formattedLastFirstDay} - ${formattedLastLastDay}`;
    this.semana = `${formattedFirstDay}  ${formattedLastDay}`;
    return this.semana;
  }


  formatearFechas(intervalo: any) {
    const aux = intervalo.split(' - ');
    const fechaInicio = parse(aux[0], 'dd/MM/yyyy', new Date());
    const fechaFin = parse(aux[1], 'dd/MM/yyyy', new Date());
    const formattedFirstDay = format(fechaInicio, 'MMM d', { locale: es });
    const formattedLastDay = format(fechaFin, 'MMM d', { locale: es });
    return `${formattedFirstDay}  ${formattedLastDay}`;
  }

  obtenerReuniones() {
    this._reunion.obtenerDatosReunion(this.idReunion).subscribe(
      (response: any) => {
        this.participantes = response.map((datos: any) => ({
          idUsuario: datos.idUsuario,
          nombre: datos.nombreUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario,
          pfp: datos.imagenUsuario
        }));
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  async obtenerIndicadores() {
    this.measurables = null
    this.measurables = await this._reunion.obtenerIndicadores(this.idReunion).toPromise();
    this.measurables = this.measurables.filter((m: { statusMeasurable: string; }) => m.statusMeasurable === 'Activo');
    function compare(a: any, b: any): number {
      return a.idMeasurable.split("M")[1] - b.idMeasurable.split("M")[1];
    }
    this.measurables.sort(compare);
    this.fechas = [];
    this.resultados = [];
    this.fechasFormateadas = [];
    this.fechasFormateadasReverso = [];
    this.fechasReverso = [];
    this.fechasRestantesTabla = [];
    this.fechasRestantes = [];
    this.datos = [];
    try {
      const auxPromises = this.measurables.map(async (measurable: any) => {
        const aux = await this._reunion.obtenerSeguimiento(measurable.idMeasurable).toPromise();
        this.datos.push(aux);
      });

      await Promise.all(auxPromises);
      for (let index = 0; index < this.datos.length; index++) {
        for (let j = 0; j < this.datos[index].length; j++) {
          const fecha = this.datos[index][j].fechaMeasurable;
          if (!this.fechas.includes(fecha)) {
            this.fechas.push(fecha);
          }
        }
      }

      if (this.fechas[0] != null) {
        const compararFechas = (a: string, b: string) => {
          const fecha1 = new Date(a.split(' - ')[0].trim().split('/').reverse().join('/'));
          const fecha2 = new Date(b.split(' - ')[0].trim().split('/').reverse().join('/'));
          return fecha1.getTime() - fecha2.getTime();
        };
        this.fechas.sort(compararFechas);
        for (let index = 0; index < this.fechas.length; index++) {
          const aux = this.formatearFechas(this.fechas[index]);
          this.fechasFormateadas.push(aux);
        }
        this.fechasFormateadasReverso = [...this.fechasFormateadas].reverse();
        this.fechasReverso = [...this.fechas].reverse();
        const ultimaFecha = this.fechas[this.fechas.length - 1].split(' - ')[0].trim();
        const ultimaFechaDate = new Date(ultimaFecha.split('/').reverse().join('/'));
        const fechaActual = new Date(this.semanaCalcular.split(' ')[0]);
        const semanasFaltantes: Date[] = [];
        for (let fecha = new Date(fechaActual); fecha > ultimaFechaDate; fecha.setDate(fecha.getDate() - 7)) {
          semanasFaltantes.push(new Date(fecha));
        }
        const semanasFaltantesTexto: string[] = semanasFaltantes.map(fecha => {
          const primeraFechaSemana = startOfWeek(fecha, { weekStartsOn: 1 });
          const ultimaFechaSemana = endOfWeek(fecha, { weekStartsOn: 1 });
          const formattedPrimeraFecha = format(primeraFechaSemana, 'dd/MM/yyyy', { locale: es });
          const formattedUltimaFecha = format(ultimaFechaSemana, 'dd/MM/yyyy', { locale: es });
          return `${formattedPrimeraFecha} - ${formattedUltimaFecha}`;
        });
        for (let index = 0; index < semanasFaltantesTexto.length; index++) {
          const aux = this.formatearFechas(semanasFaltantesTexto[index]);
          this.fechasRestantesTabla.push(aux);
        }
        const semanasFaltantesNoRepetidas = semanasFaltantesTexto.filter(semana => !this.fechas.includes(semana));
        this.fechasRestantes = semanasFaltantesNoRepetidas;
      }
      else {
        this.fechasRestantes.push(this.semanaAux)
        this.fechasRestantes.push(this.semanaPasada)
        this.fechasRestantesTabla.push(this.formatearFechas(this.semanaAux))
        this.fechasRestantesTabla.push(this.formatearFechas(this.semanaPasada))
      }

    } catch (error) {
      console.error('Error en obtenerIndicadores:', error);
    }
    this.cumplimiento();
  }

  guardarSeguimiento(idMeasurable: any, cumplimiento: any, index: any) {
    this.seguimiento.idMeasurable = idMeasurable
    this.seguimiento.cumplimientoMeasurable = cumplimiento
    this.seguimiento.fechaMeasurable = this.fechasReverso[index]
    this._reunion.crearSeguimientoIndicador(this.seguimiento).subscribe((resp: any) => {
      console.log(resp);
      this.obtenerIndicadores()
    }, err => {
      console.log(err);
    })
  }

  guardarSeguimientoRestante(idMeasurable: any, cumplimiento: any, index: any) {
    if (this.fechasRestantes[index]) {
      console.log('res');
      
      console.log(this.fechasRestantes[index]);

    } else {
      console.log(this.semana);

    }


    this.seguimiento.idMeasurable = idMeasurable
    this.seguimiento.cumplimientoMeasurable = cumplimiento
    this.seguimiento.fechaMeasurable = this.fechasRestantes[index]
    console.log(this.seguimiento);
    
    this._reunion.crearSeguimientoIndicador(this.seguimiento).subscribe((resp: any) => {
      this.obtenerIndicadores()
    }, err => {
      console.log(err);
    })
  }

  actualizarSeguimiento(idMeasurable: any, cumplimiento: any, index: any) {
    this.seguimiento.idMeasurable = idMeasurable
    this.seguimiento.cumplimientoMeasurable = cumplimiento
    this.seguimiento.fechaMeasurable = this.fechasReverso[index]
    this._reunion.actualizarSeguimientoIndicador(this.seguimiento).subscribe((resp: any) => {
      this.obtenerIndicadores()
    }, err => {
      console.log(err);
    })
  }


  async cumplimiento(): Promise<void> {
    for (let index = 0; index < this.measurables.length; index++) {
      for (let j = 0; j < this.fechasReverso.length; j++) {
        const idMeasurable = this.measurables[index].idMeasurable;
        const fecha = this.fechasReverso[j];
        const intervalo = encodeURIComponent(fecha);
        this.data = { idMeasurable, fechaMeasurable: intervalo };
        this.resultado = await this._reunion.obtenerSeguimientoFechas(this.data).toPromise();
        if (!this.resultados[idMeasurable]) {
          this.resultados[idMeasurable] = [];
        }

        if (this.resultado.length < 1) {
          this.resultados[idMeasurable].push(' ');

        } else if (this.resultado[0].cumplimientoMeasurable == 0) {
          this.resultados[idMeasurable].push('0');
        }
        else if (this.resultado[0].cumplimientoMeasurable != 0) {
          this.resultados[idMeasurable].push(this.resultado[0].cumplimientoMeasurable);
        }
      }
    }
    console.log(this.resultados);
    
  }

  abrirEditarIndicadorModal(data: any) {
    if (this.roleUsuario == 'Superadministrador' || this.roleUsuario == 'Administrador') {
      this.indicador.idMeasurable = data.idMeasurable
      this.indicador.nombreMeasurable = data.nombreMeasurable
      this.indicador.tipoMedida = data.tipoMedida
      this.indicador.goalMetric = data.goalMetric
      this.indicador.idUsuario = data.idUsuario
      this.indicador.idReunion = this.idReunion
      this._modal.open(this.editarIndicadorModal);
    }
  }

  async editarIndicador() {
    this.indicador.idReunion = this.idReunion
    if (this.indicador.goalMetric == "" || this.indicador.idUsuario == "" || this.indicador.nombreMeasurable == "" || this.indicador.tipoMedida == "") {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this._reunion.editarIndicador(this.indicador).subscribe((resp: any) => {
        if (resp.mensaje == 'Actualizado correctamente') {
          Swal.fire({
            icon: 'success',
            title: '¡Indicador editado exitosamente!',
            showConfirmButton: false,
            timer: 1500
          });
          this.obtenerIndicadores()
          this.cerrarModal()
          this.indicador = {
            idMeasurable: '',
            nombreMeasurable: '',
            tipoMedida: '',
            goalMetric: '',
            statusMeasurable: '',
            idUsuario: '',
            idReunion: ''
          };
        }
      }, (error) => {
        Swal.fire({
          icon: 'error',
          title: '¡Error!',
          text: 'Ocurrió un error.',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar'
        });
      })
    }
  }

  archivarIndicador() {
    Swal.fire({
      title: '¿Seguro que desea archivar este indicador?',
      text: '¡No podrás revertir este cambio!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Archivar'
    }).then((result) => {
      if (result.isConfirmed) {
        let data = { statusMeasurable: 'Archivado', idMeasurable: this.indicador.idMeasurable };
        this._reunion.archivarIndicador(data).subscribe(
          (resp: any) => {
            if (resp.mensaje === 'Archivado correctamente') {
              Swal.fire('Archivado!', 'Indicador archivado con éxito', 'success');
              this.obtenerIndicadores()
              this.cerrarModal()
            } else {
              Swal.fire('Error', 'No se pudo archivar el indicador', 'error');
            }
          },
          (error) => {
            console.error('Error al archivar el indicador', error);
            Swal.fire('Error', 'No se pudo archivar el indicador', 'error');
          }
        );
      }
    });
  }



  cerrarModal() {
    this._modal.dismissAll();
  }
}
