import { Component, OnInit, ViewChild, TemplateRef, AfterViewInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { format } from 'date-fns';
import { es } from 'date-fns/locale';
import { ReunionIniciadaComponent } from '../../reunion-iniciada/reunion-iniciada.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as ClassicEditor from 'ckeditor5-custom/build/ckeditor';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.css']
})
export class ActividadesComponent implements OnInit {
  @ViewChild(ReunionIniciadaComponent) iniciadaComp: ReunionIniciadaComponent | undefined;
  @ViewChild('crearProblemaModal', { static: false }) agregarProblemaModal!: TemplateRef<any>;

  idReunionIniciada: any
  idReunion: any
  todasActividades: any
  actividades: any = []
  actividadesViejas: any = []
  actividadesNuevas: any = []
  porcentaje: any
  seleccion = false
  index: any
  respDatosReunion: any
  participantes: any
  public Editor: any = ClassicEditor;

  problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '', idReunionIniciada: '' }

  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute, private _modal: NgbModal, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise()
      this.idReunion = auxiliarID[0].idReunion
      this.obtenerActividades()
      this.cdr.detectChanges()
      this.obtenerReuniones()
    });
  }

  obtenerReuniones() {
    this._reunion.obtenerDatosReunion(this.idReunion).subscribe(
      (datos: any) => {
        this.respDatosReunion = datos;
        this.participantes = datos.map((datos: any) => {
          return {
            idUsuario: datos.idUsuario,
            pfp: datos.imagenUsuario,
            nombre: datos.nombreUsuario,
            apellido: datos.apellidoUsuario,
            email: datos.emailUsuario
          };
        });
      },
      (error) => {
        console.error('Error al obtener reuniones:', error);
      }
    );
  }

  async obtenerActividades() {
    try {
      this.todasActividades = await this._reunion.obtenerActividades(this.idReunion).toPromise();
      const hoy = new Date();
      const fechaHoy: Date = new Date(hoy.toISOString().split('T')[0]);
      this.actividades = this.todasActividades
        .filter((actividad: { dueDateToDo: string; statusToDo: string; }) => {
          const fechaActividad: Date = new Date(actividad.dueDateToDo.split('T')[0]);
          const diferenciaEnDias = (fechaHoy.getTime() - fechaActividad.getTime()) / (1000 * 60 * 60 * 24);
          return !(diferenciaEnDias > 20 && actividad.statusToDo == 'Completado');
        });
      function compare(a: any, b: any): number {
        return a.idToDo.split("T")[1] - b.idToDo.split("T")[1];
      }
      this.actividades.sort(compare);
      for (let index = 0; index < this.actividades.length; index++) {
        this.actividades[index].seleccionado = 'No'
      }
      this.actividadesViejas = this.actividades.filter((actividad: { idReunionIniciada: any; }) => actividad.idReunionIniciada !== this.idReunionIniciada);
      this.actividadesNuevas = this.actividades.filter((actividad: { idReunionIniciada: any; }) => actividad.idReunionIniciada === this.idReunionIniciada);
      this.obtenerPorcentaje();
    } catch (error) {
      console.error('Error al obtener actividades:', error);
    }
  }


  obtenerPorcentaje() {
    const aux = this.actividadesViejas.filter((item: { statusToDo: string; }) => item.statusToDo === 'Completado').length;
    this.porcentaje = Math.floor((aux * 100) / this.actividadesViejas.length) || 0;
  }

  mostrarDetalles(index: number) {
    for (let index = 0; index < this.actividades.length; index++) {
      this.actividades[index].seleccionado = 'No'
    }
    this.actividades[index].seleccionado = 'Si'
    this.seleccion = true;
    this.index = index;
    return this.index;
  }

  actualizarFecha(fecha: any) {
    const data = { idToDo: this.actividades[this.index].idToDo, dueDateToDo: fecha };
    this._reunion.actualizarFechaActividad(data).subscribe(
      (resp: any) => {
        this.obtenerActividades();
      },
      (error) => {
        console.error('Error al actualizar usuario:', error);
      }
    );
  }

  actualizarUsuario(datos: any) {
    const data = { idToDo: this.actividades[this.index].idToDo, idUsuario: datos };
    this._reunion.actualizarUsuarioActividad(data).subscribe(
      (resp: any) => {
        this.obtenerActividades();
      },
      (error) => {
        console.error('Error al actualizar usuario:', error);
      }
    );
  }

  abrirNuevoProblemaModal(data: any) {
    this.problema.detallesIssue = "<p><i>Contexto actividad: " + data.tituloToDo + " </i></p>" + data.detallesToDo;
    this.problema.idUsuario = data.idUsuario;
    this._modal.open(this.agregarProblemaModal);
  }

  agregarProblema() {
    if (this.problema.nombreIssue === '' || this.problema.idUsuario === '') {
      alert('Llene los campos');
    } else {
      this.problema.idReunion = this.idReunion;
      this.problema.statusIssue = 'Activo';
      const fechaHoy = new Date();
      const fechaSolo = fechaHoy.toISOString().split('T')[0];
      this.problema.fechaIssue = fechaSolo;
      this.problema.idReunionIniciada = this.idReunionIniciada
      this._reunion.crearProblema(this.problema).subscribe(
        (resp: any) => {
          Swal.fire({
            icon: 'success',
            title: 'Problema creado exitosamente!',
            showConfirmButton: false,
            timer: 1500
          });
          this.problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '', idReunionIniciada: '' };
          this.obtenerActividades();
          this.cerrarModal();
        },
        (error) => {
          console.error('Error al crear el problema:', error);
          Swal.fire({
            icon: 'error',
            title: 'Error al crear el problema',
            text: 'Hubo un error al crear el problema. Por favor, inténtalo de nuevo.'
          });
        }
      );
    }
  }

  async completado(data: any) {
    try {
      if (data.statusToDo === 'Activo') {
        data.statusToDo = 'Completado';
        await this._reunion.actualizarStatusActividad(data).toPromise();
        this.obtenerPorcentaje();
      } else if (data.statusToDo === 'Completado') {
        data.statusToDo = 'Activo';
        await this._reunion.actualizarStatusActividad(data).toPromise();
        this.obtenerPorcentaje();
      }
    } catch (error) {
      console.error('Error al completar la actividad:', error);
    }
  }

  tarde(fecha: any) {
    const fechaHoy = new Date();
    const fechaParametro = new Date(fecha);
    if (fechaParametro < fechaHoy) {
      return true;
    } else {
      return false;
    }
  }

  cerrarModal() {
    this._modal.dismissAll();
  }
}
