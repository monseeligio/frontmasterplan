import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2'; import { EventEmitter, Output } from '@angular/core';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import emailjs from '@emailjs/browser'
import { ReunionIniciadaComponent } from '../../reunion-iniciada/reunion-iniciada.component';

@Component({
  selector: 'app-conclusion',
  templateUrl: './conclusion.component.html',
  styleUrls: ['./conclusion.component.css']
})
export class ConclusionComponent {
  @ViewChild(ReunionIniciadaComponent) iniciadaComp: ReunionIniciadaComponent | undefined;


  idReunionIniciada: any
  minutos: any
  idReunion: any
  todasActividades: any
  actividades: any = []
  actividadesViejas: any = []
  actividadesNuevas: any = []
  porcentaje: any
  respDatosReunion: any
  participantes: any = []
  calificaciones: number[] = [];
  promedio = 0
  finalizada = false
  issuesResueltos: any
  issuesAgrupados: { [idUsuario: string]: any[] } = {};
  headlinesVistos: { [idUsuario: string]: any[] } = {};
  tiempo: any
  actividadesAgrupadas: { [idUsuario: string]: any[] } = {};
  userID = 'TU_USER_ID';
  templateID = 'template_exfpqc8';
  serviceID = 'service_goarrvn';


  constructor(private _reunion: ReunionesService, private route: ActivatedRoute, private _router: Router) { }

  async ngOnInit() {
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero;

      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise();
      this.idReunion = auxiliarID[0].idReunion;

      await this.obtenerActividades();
      await this.obtenerReuniones();
      this.minutos = auxiliarID[0].minutoReunionIniciada
      // await this.terminarReunion();
    });
  }


  async obtenerReuniones() {
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          nombre: datos.nombreUsuario,
          pfp: datos.imagenUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario
        };
      });
    } catch (error) {
      console.log(error);
    }
  }


  async obtenerActividades() {
    try {
      this.todasActividades = await this._reunion.obtenerActividades(this.idReunion).toPromise();
      const hoy = new Date();
      const fechaHoy: Date = new Date(hoy.toISOString().split('T')[0]);
      this.actividades = this.todasActividades
        .filter((actividad: { dueDateToDo: string; statusToDo: string; }) => {
          const fechaActividad: Date = new Date(actividad.dueDateToDo.split('T')[0]);
          const diferenciaEnDias = (fechaHoy.getTime() - fechaActividad.getTime()) / (1000 * 60 * 60 * 24);
          return !(diferenciaEnDias > 10 && actividad.statusToDo == 'Completado');
        });
      function compare(a: any, b: any): number {
        return a.idToDo.split("T")[1] - b.idToDo.split("T")[1];
      }
      this.actividades.sort(compare);
      this.actividadesViejas = this.actividades.filter((actividad: { idReunionIniciada: any; }) => actividad.idReunionIniciada !== this.idReunionIniciada);
      this.actividadesNuevas = this.actividades.filter((actividad: { idReunionIniciada: any; }) => actividad.idReunionIniciada === this.idReunionIniciada);
      this.obtenerPorcentaje();
      this.actividades = this.todasActividades
        .filter((actividad: { dueDateToDo: string; idReunionIniciada: string; statusToDo: string; }) => {
          const fechaParametro = new Date(actividad.dueDateToDo);
          return (
            actividad.idReunionIniciada === this.idReunionIniciada ||
            actividad.statusToDo !== 'Completado'
          );
        });
    } catch (error) {
      console.error('Error al obtener actividades:', error);
    }
  }

  obtenerPorcentaje() {
    const aux = this.actividadesViejas.filter((item: { statusToDo: string; }) => item.statusToDo === 'Completado').length;
    this.porcentaje = Math.floor((aux * 100) / this.actividadesViejas.length) || 0;
  }

  tarde(fecha: any) {
    const fechaHoy = new Date();
    const fechaParametro = new Date(fecha);
    if (fechaParametro < fechaHoy) {
      return true;
    } else {
      return false;
    }
  }

  calcularProm() {
    const sumatoria = this.calificaciones.reduce((total, calificacion) => total + calificacion, 0);
    return this.promedio = Math.floor(sumatoria / this.calificaciones.length);
  }

  formatoFecha(fecha: string): string {
    const fechaObj = new Date(fecha);
    const dia = fechaObj.getDate();
    const mes = fechaObj.getMonth() + 1;
    const año = fechaObj.getFullYear();
    return `${dia.toString().padStart(2, '0')}/${mes.toString().padStart(2, '0')}/${año}`;
  }

  async terminarReunion() {
    if (this.minutos < 5) {
      let dataFin = { statusReunion: 'Activa', idReunion: this.idReunion }
      this._reunion.actualizarStatusReunion(dataFin).toPromise()
      this._router.navigate(['reuniones']);
    } else {
      this.finalizada = true
      this.calcularProm()
      let status = { statusHeadline: 'Visto', idReunionIniciada: this.idReunionIniciada }
      this._reunion.actualizarStatusAsunto(status).toPromise()
      const aux: any = await this._reunion.obtenerAsuntosReunionIniciada(this.idReunionIniciada).toPromise();
      const headlinesAgrupados: { [idUsuario: string]: any[] } = {};
      aux.forEach((headline: { idUsuario: any }) => {
        const idUsuario = headline.idUsuario;
        if (!headlinesAgrupados[idUsuario]) {
          headlinesAgrupados[idUsuario] = [];
        }
        headlinesAgrupados[idUsuario].push(headline);
      });
      this.headlinesVistos = headlinesAgrupados;
      const actividadesAgrupadas: { [idUsuario: string]: any[] } = {};
      this.actividades.forEach((headline: { idUsuario: any }) => {
        const idUsuario = headline.idUsuario;
        if (!actividadesAgrupadas[idUsuario]) {
          actividadesAgrupadas[idUsuario] = [];
        }
        actividadesAgrupadas[idUsuario].push(headline);
      });
      this.actividadesAgrupadas = actividadesAgrupadas;
      let issues: any = await this._reunion.obtenerProblemasReunionIniciada(this.idReunionIniciada).toPromise()
      const issuesAgrupados: { [idUsuario: string]: any[] } = {};
      issues.forEach((problema: { idUsuario: string }) => {
        const idUsuario = problema.idUsuario;
        if (!issuesAgrupados[idUsuario]) {
          issuesAgrupados[idUsuario] = [];
        }
        issuesAgrupados[idUsuario].push(problema);
      });
      this.issuesAgrupados = issuesAgrupados
      this.issuesResueltos = issues.length
      this.tiempo = await this._reunion.obtenerTiempoReunionIniciada(this.idReunionIniciada).toPromise()
      this.tiempo = this.tiempo[0].minutoReunionIniciada
      let promedio = this.calcularProm()
      let participantes: any = []
      for (let index = 0; index < this.participantes.length; index++) {
        participantes.push(this.participantes[index].email)

      }
      let data = {
        participantes: participantes, nombreReunion: this.respDatosReunion[0].nombreReunion,
        issuesResueltos: this.issuesResueltos, porcentaje: this.porcentaje,
        promedio: promedio, tiempo: this.tiempo, actividades: this.actividadesAgrupadas,
        asuntos: this.headlinesVistos, problemas: this.issuesAgrupados
      }
      this._reunion.enviarCorreo(data).subscribe((resp: any) => {
      }, err => {
        console.log(err);
      })
      let dataFin = { statusReunion: 'Activa', idReunion: this.idReunion }
      this._reunion.actualizarStatusReunion(dataFin).toPromise()
    }
  }
}