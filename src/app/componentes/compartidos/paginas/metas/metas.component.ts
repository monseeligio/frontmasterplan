import { Component, OnInit, ChangeDetectorRef, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApirestService } from 'src/app/servicios/apirest.service';
import { ReunionesService } from 'src/app/servicios/reuniones.service';
import { format, isWeekend, parse } from 'date-fns';
import { es } from 'date-fns/locale';
import { ReunionIniciadaComponent } from '../../reunion-iniciada/reunion-iniciada.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as ClassicEditor from 'ckeditor5-custom/build/ckeditor';
import jwt_decode from "jwt-decode";
import Swal from 'sweetalert2'; import { EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-metas',
  templateUrl: './metas.component.html',
  styleUrls: ['./metas.component.css']
})
export class MetasComponent implements OnInit {
  @ViewChild('editarMetaModal', { static: false }) editarMetaModal!: TemplateRef<any>;

  roleUsuario: any
  tokenPayload: any;
  idReunionIniciada: any
  idReunion: any
  respDatosReunion: any
  participantes: any
  hoy: any
  metas: any
  seguimientos: any = []
  activos = 0
  mesSeleccionado: any;
  anioSeleccionado: number = 0
  semanasHabiles: any = []
  fechasTabla: any = []



  @Output() metasActualizadas = new EventEmitter<any[]>();
  @ViewChild(ReunionIniciadaComponent) iniciadaComp: ReunionIniciadaComponent | undefined;
  @ViewChild('crearProblemaModal', { static: false }) agregarProblemaModal!: TemplateRef<any>;
  @ViewChild('crearActividadModal', { static: false }) agregarActividadModal!: TemplateRef<any>;


  statusGoal: any
  seleccion = false
  index: any

  public Editor: any = ClassicEditor;

  problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '' }
  actividad: any = { idToDo: '', tituloToDo: '', detallesToDo: '', fechaCreadoToDo: '', dueDateToDo: '', statusToDo: '', idUsuario: [], idReunion: '' }
  meta = { idGoal: '', tituloGoal: '', statusGoal: '', realGoal: '', acumuladoGoal: '', porcentajeGoal: 0, idUsuario: '', idReunion: '' };


  constructor(private _service: ApirestService, private _reunion: ReunionesService, private _router: Router, private route: ActivatedRoute, private _modal: NgbModal) { }

  async ngOnInit() {
    this.token();
    this.route.params.subscribe(async params => {
      const idReunionString = params['idReunionIniciada'];
      const idNumero = parseInt(idReunionString.match(/\d+/)[0]);
      this.idReunionIniciada = 'SR' + idNumero;
      const auxiliarID: any = await this._reunion.obtenerReunionDeIniciada(this.idReunionIniciada).toPromise();
      this.idReunion = auxiliarID[0].idReunion;
      try {
        await Promise.all([this.obtenerMetas(), this.obtenerSemanas()]);
      } catch (error) {
        console.error('Error en la carga de datos:', error);
      }

      this.obtenerReuniones();
    });
  }

  token() {
    const token: any = localStorage.getItem('token')
    this.tokenPayload = jwt_decode(token);
    this.roleUsuario = this.tokenPayload.roleUsuario;
  }

  async obtenerReuniones() {
    this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
    try {
      this.respDatosReunion = await this._reunion.obtenerDatosReunion(this.idReunion).toPromise();
      this.participantes = this.respDatosReunion.map((datos: any) => {
        return {
          idUsuario: datos.idUsuario,
          nombre: datos.nombreUsuario,
          apellido: datos.apellidoUsuario,
          email: datos.emailUsuario
        };
      });
    } catch (error) {
      console.log(error);
    }
  }

  async obtenerMetas() {
    this.metas = [];
    this.hoy = new Date();
    try {
      this.metas = await this._reunion.obtenerMetas(this.idReunion).toPromise();
      this.metas = this.metas.filter((m: { statusGoal: string; }) => m.statusGoal === 'Activo');
      function compare(a: any, b: any): number {
        return a.idGoal.split("G")[1] - b.idGoal.split("G")[1];
      }
      this.metas.sort(compare);
      await Promise.all(this.metas.map(async (meta: any) => {
        if (meta.statusGoal == 'Activo') {
          this.activos++;
        }
        let mesRoca = await this.obtenerPlaneado(meta.idGoal);
        if (mesRoca !== null) {
          meta.planeadoMes = mesRoca.planeadoGoal;
          meta.realMes = mesRoca.realMesGoal;
        } else {
          meta.planeadoMes = mesRoca;
          meta.realMes = mesRoca;
        }
      }));
    } catch (error) {
      console.error('Error al obtener metas:', error);
    }
    await this.obtenerSeguimiento();
  }


  formatearFechas(intervalo: any) {
    if (intervalo.length > 10) {
      const aux = intervalo.split(' - ');
      const fechaInicio = parse(aux[0], 'dd/MM/yyyy', new Date());
      const fechaFin = parse(aux[1], 'dd/MM/yyyy', new Date());
      const formattedFirstDay = format(fechaInicio, 'MMM d', { locale: es });
      const formattedLastDay = format(fechaFin, 'MMM d', { locale: es });
      return `${formattedFirstDay}  ${formattedLastDay}`;
    } else {
      const fechaInicio = parse(intervalo, 'dd/MM/yyyy', new Date());
      const formattedFirstDay = format(fechaInicio, 'MMM d', { locale: es });
      return formattedFirstDay;
    }
  }

  obtenerSemanas() {
    this.hoy = new Date()
    if (this.hoy.getMonth() == 0) {
      this.mesSeleccionado = 0
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 1) {
      this.mesSeleccionado = 1
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 2) {
      this.mesSeleccionado = 2
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 3) {
      this.mesSeleccionado = 3
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 4) {
      this.mesSeleccionado = 4
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 5) {
      this.mesSeleccionado = 5
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 6) {
      this.mesSeleccionado = 6
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 7) {
      this.mesSeleccionado = 7
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 8) {
      this.mesSeleccionado = 8
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 9) {
      this.mesSeleccionado = 9
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 10) {
      this.mesSeleccionado = 10
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    } else if (this.hoy.getMonth() == 11) {
      this.mesSeleccionado = 11
      this.anioSeleccionado = this.hoy.getFullYear()
      this.calcularSemanas(this.hoy.getMonth(), this.hoy.getFullYear())
    }
  }

  calcularSemanas(mes: number, anio: number) {
    let primerDiaMes = new Date(anio, mes, 1);
    let ultimoDiaMes = new Date(anio, mes + 1, 0);

    let fechaActual = primerDiaMes;
    let rangosSemanales = [];
    this.semanasHabiles = [];
    this.fechasTabla = []
    while (fechaActual <= ultimoDiaMes) {
      if (!isWeekend(fechaActual)) {
        rangosSemanales.push(format(fechaActual, 'dd/MM/yyyy'));
      }

      fechaActual = new Date(fechaActual.getTime() + 86400000);
      if ((fechaActual.getDay() === 1 && rangosSemanales.length > 0) || fechaActual > ultimoDiaMes) {
        if (rangosSemanales.length > 1) {
          const rangoSemana = rangosSemanales[0] + ' - ' + rangosSemanales[rangosSemanales.length - 1];
          this.semanasHabiles.push(rangoSemana);
        } else if (rangosSemanales.length === 1) {
          this.semanasHabiles.push(rangosSemanales[0]);
        }
        rangosSemanales = [];
      }
    }
    for (let index = 0; index < this.semanasHabiles.length; index++) {
      const aux = this.formatearFechas(this.semanasHabiles[index]);
      this.fechasTabla.push(aux);
    }
  }

  seleccionarMes() {
    this.metas = []
    this.obtenerMetas()
    this.calcularSemanas(+this.mesSeleccionado, +this.anioSeleccionado)
  }

  async obtenerPlaneado(idGoal: any) {
    try {
      const resp: any = await this._reunion.obtenerPlaneadoMes((+this.mesSeleccionado + " " + +this.anioSeleccionado), idGoal).toPromise();
      if (resp.length > 0) {
        return resp[0];
      } else {
        return null
      }
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  guardarPlaneadoMes(idGoal: any, planeado: any) {
    if (planeado < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'No se aceptan valores negativos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      let data = { mesAnioPlan: +this.mesSeleccionado + " " + +this.anioSeleccionado, planeadoGoal: +planeado, realMesGoal: 0, idGoal: idGoal }
      this._reunion.guardarPlaneadoMes(data).subscribe((resp: any) => {
        if (resp.mensaje == 'Registrado correctamente') {
          this.obtenerMetas()
        }
      }, err => {
        console.log(err);
      })
    }
  }

  actualizarPlaneadoMes(idGoal: any, planeado: any) {
    if (planeado < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'No se aceptan valores negativos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      let data = { mesAnioPlan: +this.mesSeleccionado + " " + +this.anioSeleccionado, planeadoGoal: +planeado, realMesGoal: 0, idGoal: idGoal }
      this._reunion.actualizarPlaneadoMes(data).subscribe((resp: any) => {
        if (resp.mensaje == 'Actualizado correctamente') {
          this.obtenerMetas()
        }
      }, err => {
        console.log(err);
      })
    }
  }

  async obtenerSeguimiento() {
    this.seguimientos = [];
    try {
      await Promise.all(
        this.metas.map(async (meta: { idGoal: string | number; }, i: any) => {
          this.seguimientos[meta.idGoal] = [];
          await Promise.all(
            this.semanasHabiles.map(async (semana: string | number | boolean, index: any) => {
              try {
                let aux: any = await this._reunion
                  .obtenerSeguimientoRocas(encodeURIComponent(semana), meta.idGoal)
                  .toPromise();
                if (aux.length > 0) {
                  this.seguimientos[meta.idGoal].push(+aux[0].cumplimientoGoal);
                } else {
                  this.seguimientos[meta.idGoal].push(null);
                }
              } catch (error) {
                console.error('Error al obtener seguimiento:', error);
              }
            })
          );
          this.obtenerComentario(i, meta.idGoal);
        })
      );
    } catch (error) {
      console.error('Error al obtener seguimiento:', error);
    }
  }


  guardarSeguimiento(index: any, cumplimiento: any, Goal: any) {
    if (+cumplimiento < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'No se aceptan valores negativos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      let data = { fechaGoal: this.semanasHabiles[index], cumplimientoGoal: +cumplimiento, comentarios: '', idGoal: Goal.idGoal }
      this._reunion.guardarSeguimiento(data).subscribe((resp: any) => {
        if (resp.mensaje == 'Registrado correctamente') {
          if (Goal.acumuladoGoal == 'Si' && Goal.realGoal == 'Si') {
            this.sumarPorcentaje(+cumplimiento, Goal.idGoal)
            this.sumarReal(+cumplimiento, Goal.idGoal)
            this.obtenerMetas()
          } else if (Goal.acumuladoGoal == 'Si' && Goal.realGoal == 'No') {
            this.sumarPorcentaje(+cumplimiento, Goal.idGoal)
            this.obtenerMetas()
          } else if (Goal.acumuladoGoal == 'No' && Goal.realGoal == 'Si') {
            this.sumarReal(+cumplimiento, Goal.idGoal)
            this.obtenerMetas()
          } else {
            this.obtenerMetas()
          }
        }
      }, err => {
        console.log(err);
      })
    }
  }

  actualizarSeguimiento(index: any, cumplimiento: any, Goal: any) {
    if (+cumplimiento < 0) {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'No se aceptan valores negativos',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      let data = { fechaGoal: this.semanasHabiles[index], cumplimientoGoal: +cumplimiento, comentarios: 'hola', idGoal: Goal.idGoal }
      this._reunion.actualizarSeguimiento(data).subscribe((resp: any) => {
        if (resp.mensaje == 'Actualizado correctamente') {
          if (Goal.acumuladoGoal === 'Si' && Goal.realGoal === 'Si') {
            this.actualizarReal(+this.seguimientos[Goal.idGoal][index], +cumplimiento, Goal.idGoal);
            this.actualizarPorcentaje(+this.seguimientos[Goal.idGoal][index], +cumplimiento, Goal.idGoal);
            this.obtenerMetas()
          } else if (Goal.acumuladoGoal === 'Si' && Goal.realGoal === 'No') {
            this.actualizarPorcentaje(+this.seguimientos[Goal.idGoal][index], +cumplimiento, Goal.idGoal);
            this.obtenerMetas()
          } else if (Goal.acumuladoGoal === 'No' && Goal.realGoal === 'Si') {
            this.actualizarReal(+this.seguimientos[Goal.idGoal][index], cumplimiento, Goal.idGoal);
            this.obtenerMetas()
          } else {
            this.obtenerMetas()
          }
        }
      }, err => {
        console.log(err);
      })
    }
  }

  sumarReal(cumplimiento: any, idGoal: any) {
    console.log(cumplimiento);
    let data = { cumplimientoGoal: +cumplimiento, idGoal: idGoal }
    this._reunion.actualizarRealMes(data).subscribe((resp: any) => {
    }, err => {
      console.log(err);
    })
  }

  actualizarReal(datoAnterior: number, cumplimiento: number, idGoal: any) {
    let data = { datoAnterior: +datoAnterior, cumplimientoGoal: +cumplimiento, idGoal: idGoal }
    this._reunion.actualizarRealMesModificado(data).subscribe((resp: any) => {
    }, err => {
      console.log(err);
    })
  }

  sumarPorcentaje(cumplimiento: number, idGoal: any) {
    let data = { sumaPorcentaje: cumplimiento, idGoal: idGoal }
    this._reunion.actualizarPorcentaje(data).subscribe((resp: any) => {
    }, err => {
      console.log(err);
    })
  }

  actualizarPorcentaje(datoAnterior: number, cumplimiento: number, idGoal: any) {
    let data = { datoAnterior: +datoAnterior, sumaPorcentaje: +cumplimiento, idGoal: idGoal }
    this._reunion.actualizarPorcentajeModificado(data).subscribe((resp: any) => {
    }, err => {
      console.log(err);
    })
  }

  async obtenerComentario(index: any, idGoal: any) {
    let aux: any
    if (this.seguimientos[idGoal][0] == null) {
      this._reunion.obtenerComentarios(encodeURIComponent(this.semanasHabiles[0]), idGoal).subscribe((resp: any) => {
        if (resp.length > 0) {
          this.metas[index].comentariosGoal = resp[0].comentariosGoal
        } else {
          this.metas[index].comentariosGoal = ''
        }

      }, err => {
        console.log(err);
      })
    } else {
      for (let index = 1; index < this.seguimientos[idGoal].length; index++) {
        if (this.seguimientos[idGoal][index] == null) {
          aux = index - 1
          break;
        } if ((index + 1) == this.seguimientos[idGoal].length) {
          aux = index
        }
      }
      this._reunion.obtenerComentarios(encodeURIComponent(this.semanasHabiles[aux]), idGoal).subscribe((resp: any) => {
        if (resp.length > 0) {
          this.metas[index].comentariosGoal = resp[0].comentariosGoal
        } else {
          this.metas[index].comentariosGoal = ''
        }
      }, err => {
        console.log(err);
      })
    }

  }

  guardarComentario(comentario: any, idGoal: any) {
    let aux: any
    if (this.seguimientos[idGoal][0] == null) {
      let data = { comentariosGoal: comentario, fechaGoal: this.semanasHabiles[0], idGoal: idGoal }
      this._reunion.agregarComentario(data).subscribe((resp: any) => {
        this.obtenerMetas()
      }, err => {
        console.log(err);
      })
    } else {
      for (let index = 1; index < this.seguimientos[idGoal].length; index++) {
        if (this.seguimientos[idGoal][index] == null) {
          aux = index - 1
          break;
        } if ((index + 1) == this.seguimientos[idGoal].length) {
          aux = index
        }
      }
      let data = { comentariosGoal: comentario, fechaGoal: this.semanasHabiles[aux], idGoal: idGoal }
      this._reunion.agregarComentario(data).subscribe((resp: any) => {
        this.obtenerMetas()
      }, err => {
        console.log(err);
      })
    }
  }

  abrirEditarMetaModal(data: any) {
    if (this.roleUsuario == 'Superadministrador' || this.roleUsuario == 'Administrador') {
      this.meta.idGoal = data.idGoal
      this.meta.tituloGoal = data.tituloGoal
      this.meta.statusGoal = data.statusGoal
      this.meta.idUsuario = data.idUsuario
      this._modal.open(this.editarMetaModal);
    }
  }

  async editarMeta() {
    this.meta.idReunion = this.idReunion
    if (this.meta.tituloGoal == "" || this.meta.idUsuario == "") {
      Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Llene todos los campos.',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this._reunion.editarMeta(this.meta).subscribe((resp: any) => {
        if (resp.mensaje == 'Actualizado correctamente') {
          Swal.fire({
            icon: 'success',
            title: '¡Indicador editado exitosamente!',
            showConfirmButton: false,
            timer: 1500
          });
          this.obtenerMetas()
          this.cerrarModal()
          this.meta = {
            idGoal: '',
            tituloGoal: '', statusGoal: '', realGoal: '', acumuladoGoal: '',
            porcentajeGoal: 0, idUsuario: '', idReunion: ''
          };
        }
      }, (error) => {
        Swal.fire({
          icon: 'error',
          title: '¡Error!',
          text: 'Ocurrió un error.',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar'
        });
      })
    }
  }

  archivarMeta() {
    Swal.fire({
      title: '¿Seguro que desea archivar este indicador?',
      text: '¡No podrás revertir este cambio!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Archivar'
    }).then((result) => {
      if (result.isConfirmed) {
        let data = { statusGoal: 'Archivado', idGoal: this.meta.idGoal };
        this._reunion.archivarMeta(data).subscribe(
          (resp: any) => {
            if (resp.mensaje === 'Archivado correctamente') {
              Swal.fire('Archivado!', 'Roca archivada con éxito', 'success');
              this.obtenerMetas()
              this.cerrarModal()
            } else {
              Swal.fire('Error', 'No se pudo archivar la roca', 'error');
            }
          },
          (error) => {
            console.error('Error al archivar la roca', error);
            Swal.fire('Error', 'No se pudo archivar la roca', 'error');
          }
        );
      }
    });
  }


  abrirNuevoProblemaModal(data: any) {
    this.problema.detallesIssue = data.detallesGoal
    this.problema.idUsuario = data.idUsuario
    this._modal.open(this.agregarProblemaModal);
  }

  agregarProblema() {
    this.problema.idReunion = this.idReunion
    this.problema.statusIssue = 'Activo'
    let fechaHoy = new Date();
    let fechaSolo = fechaHoy.toISOString().split('T')[0];
    this.problema.fechaIssue = fechaSolo
    if (this.problema.nombreIssue == '' || this.problema.idUsuario == '') {
      alert('Llene los campos')
    } else {
      this._reunion.crearProblema(this.problema).subscribe((resp: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Problema creado exitosamente!',
          showConfirmButton: false,
          timer: 1500
        });
        this.problema = { idIssue: '', nombreIssue: '', detallesIssue: '', fechaIssue: '', statusIssue: '', idUsuario: '', idReunion: '' }
        this.cerrarModal()
      }, err => {
        console.log(err);
      })
    }
  }

  abrirNuevaActividadModal(data: any) {
    this.actividad.detallesToDo = data.detallesGoal
    this.actividad.idUsuario.push(data.idUsuario)
    this._modal.open(this.agregarActividadModal);
  }

  async agregarActividad() {
    this.actividad.idReunion = this.idReunion
    this.actividad.statusToDo = 'Activo'
    let fechaHoy = new Date();
    let fechaSolo = fechaHoy.toISOString().split('T')[0];
    this.actividad.fechaCreadoToDo = fechaSolo
    if (this.actividad.tituloToDo == '' || this.actividad.dueDateToDo == '' || this.actividad.idUsuario.length < 0) {
      alert('Llene los campos')
    } else {
      for (let index = 0; index < this.actividad.idUsuario.length; index++) {
        const data = { tituloToDo: this.actividad.tituloToDo, detallesToDo: this.actividad.detallesToDo, fechaCreadoToDo: this.actividad.fechaCreadoToDo, dueDateToDo: this.actividad.dueDateToDo, statusToDo: this.actividad.statusToDo, idUsuario: this.actividad.idUsuario[index], idReunion: this.idReunion }
        await this._reunion.crearActividad(data).toPromise()
      }
      Swal.fire({
        icon: 'success',
        title: 'Se creó la actividad con éxito',
        showConfirmButton: false,
        timer: 1500
      });
      this.cerrarModal()
    }

  }

  cerrarModal() {
    this._modal.dismissAll();
  }
}
