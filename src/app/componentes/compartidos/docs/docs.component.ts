import { Component, HostListener, OnInit, TemplateRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import jwt_decode from "jwt-decode";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.css']
})
export class DocsComponent implements OnInit {
  @ViewChild('crearCarpetaModal', { static: false }) crearCarpetaModal!: TemplateRef<any>;
  @ViewChild('moverArchivoModal', { static: false }) moverArchivoModal!: TemplateRef<any>;
  @ViewChild('renombrarArchivoModal', { static: false }) renombrarArchivoModal!: TemplateRef<any>;

  roleUsuario: any
  archivos: any = [];
  archivosMover: any = [];
  historialNavegacion: string[] = ['Documentos'];
  historialNavegacionMover: string[] = ['Documentos'];
  carpeta: any = ''
  nuevoNombre: any = ''
  archivo: any
  elementoSeleccionado: any
  menuVisible = false;
  menuTop: any;
  menuLeft: any;


  constructor(private http: HttpClient, private _modal: NgbModal, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    const token: any = localStorage.getItem('token');
    const tokenPayload: any = jwt_decode(token);
    this.roleUsuario = tokenPayload.roleUsuario;
    this.listaArchivos();

  }

  ordenarNumerosYTexto(a: string, b: string): number {
    const partesA = a.split(/(\d+)|(\D+)/).filter(Boolean);
    const partesB = b.split(/(\d+)|(\D+)/).filter(Boolean);
    for (let i = 0; i < Math.max(partesA.length, partesB.length); i++) {
      const numA = parseInt(partesA[i], 10);
      const numB = parseInt(partesB[i], 10);
      if (!isNaN(numA) && !isNaN(numB)) {
        if (numA !== numB) {
          return numA - numB;
        }
      } else {
        const comparacion = partesA[i].localeCompare(partesB[i]);
        if (comparacion !== 0) {
          return comparacion;
        }
      }
    }
    return 0;
  }


  obtenerElementos(nombreCarpeta: string) {
    let aux = '';
    for (let index = 1; index < this.historialNavegacion.length; index++) {
      aux = aux + this.historialNavegacion[index] + '%2F'
    }
    this.http.get<any>(`http://149.56.248.111:5057/archivo/carpeta/${aux}`).subscribe(
      response => {
        this.archivos = response.archivos;
        this.archivos.sort(this.ordenarNumerosYTexto)
      },
      error => {
        console.error(error);
      }
    );
  }

  obtenerElementosMover(nombreCarpeta: string) {
    let aux = '';
    for (let index = 1; index < this.historialNavegacionMover.length; index++) {
      aux = aux + this.historialNavegacionMover[index] + '%2F'
    }
    this.http.get<any>(`http://149.56.248.111:5057/archivo/carpeta/${aux}`).subscribe(
      response => {
        this.archivosMover = response.archivos;
        this.archivosMover.sort(this.ordenarNumerosYTexto)
      },
      error => {
        console.error(error);
      }
    );
  }


  listaArchivos() {
    this.http.get<any>('http://149.56.248.111:5057/archivo/listaArchivos').subscribe(
      response => {
        this.archivos = response.archivos;
        this.archivos.sort(this.ordenarNumerosYTexto)
      },
      error => {
        console.error(error);
      }
    );
  }

  listaArchivosMover() {
    this.http.get<any>('http://149.56.248.111:5057/archivo/listaArchivos').subscribe(
      response => {
        this.archivosMover = response.archivos;
        this.archivosMover.sort(this.ordenarNumerosYTexto)
      },
      error => {
        console.error(error);
      }
    );
  }

  navegarA(index: number) {
    const carpeta = this.historialNavegacion[index];
    this.historialNavegacion = this.historialNavegacion.slice(0, index + 1);
    if (index === 0) {
      this.listaArchivos();
    } else {
      this.obtenerElementos(carpeta);
    }
  }

  navegarAMover(index: number) {
    const carpeta = this.historialNavegacionMover[index];
    this.historialNavegacionMover = this.historialNavegacionMover.slice(0, index + 1);
    if (index === 0) {
      this.listaArchivosMover();
    } else {
      this.obtenerElementosMover(carpeta);
    }
  }

  esArchivo(nombre: string): boolean {
    const extensionesPermitidas = ['.doc', '.docx', '.xml', '.xls', '.xlsx', '.pdf', '.txt', '.ppt', '.pptx'];
    return extensionesPermitidas.includes(this.obtenerExtension(nombre));
  }


  esCarpeta(nombre: string): boolean {
    return !this.esArchivo(nombre);
  }

  obtenerExtension(nombre: string): string {
    if (typeof nombre !== 'string') {
      return '';
    }
    return nombre.slice(nombre.lastIndexOf('.'));
  }

  obtenerUrlArchivo(archivo: any) {
    let aux = '';
    for (let index = 1; index < this.historialNavegacion.length; index++) {
      aux = aux + this.historialNavegacion[index] + '/'
    }
    aux = aux + archivo
    return `http://149.56.248.111:5057/archivos/${aux}`;
  }

  descargarArchivo(nombreArchivo: string) {
    const url = this.obtenerUrlArchivo(nombreArchivo);
    const anchor = document.createElement('a');
    anchor.href = url;
    anchor.download = nombreArchivo;
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  }

  elementoDobleClick(elemento: string) {
    if (this.esCarpeta(elemento)) {
      this.abrirCarpeta(elemento, true);
    } else {
      this.descargarArchivo(elemento)
    }
  }

  elementoDobleClickMover(elemento: string) {
    if (this.esCarpeta(elemento)) {
      this.abrirCarpetaMover(elemento, true);
    }
  }

  abrirCarpeta(nombreCarpeta: string, dobleClic: boolean = false) {
    if (dobleClic || this.historialNavegacion.length <= 1) {
      this.historialNavegacion.push(nombreCarpeta);
      this.obtenerElementos(nombreCarpeta);
    }
  }

  abrirCarpetaMover(nombreCarpeta: string, dobleClic: boolean = false) {
    if (dobleClic || this.historialNavegacionMover.length <= 1) {
      this.historialNavegacionMover.push(nombreCarpeta);
      this.obtenerElementosMover(nombreCarpeta);
    }
  }

  selectedFile(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files && target.files.length > 0) {
      const file = target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      this.archivo = file;
      this.subirArchivo();
    }
  }

  subirArchivo() {
    if (!this.archivo) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Por favor, selecciona un archivo antes de subirlo.',
      });
    } else {
      let ruta = '';
      for (let index = 1; index < this.historialNavegacion.length; index++) {
        ruta = ruta + this.historialNavegacion[index] + '%2F';
      }
      ruta = ruta + this.carpeta;
      if (ruta === '') {
        ruta = 'Sin ruta';
      }
      const formData = new FormData();
      formData.append('archivo', this.archivo);
      this.http.post(`http://149.56.248.111:5057/archivo/subirArchivo/${ruta}`, formData).subscribe(
        (resp: any) => {
          if (this.historialNavegacion.length <= 1) {
            this.listaArchivos()
          } else {
            this.obtenerElementos(this.historialNavegacion[this.historialNavegacion.length])
          }
          this.archivo = null
          this.cdr.detectChanges()
        },
        error => {
          console.log('Error en la solicitud:', error);
          this.archivo=null
        });
    }
  }

  abrirmoverArchivoModal(elemento: any) {
    this.historialNavegacionMover = [...this.historialNavegacion];
    if (this.historialNavegacionMover.length == 1) {
      this.listaArchivosMover()
    } else {
      this.obtenerElementosMover(elemento)
    }
    this._modal.open(this.moverArchivoModal);
  }

  moverArchivo() {
    const origen = this.historialNavegacion.slice(1).join('/') + `/${this.elementoSeleccionado}`;
    const destino = this.historialNavegacionMover.slice(1).join('/') + `/${this.elementoSeleccionado}`;
    this.http.post<any>('http://149.56.248.111:5057/archivo/moverArchivo', { origen, destino }).subscribe(
      (resp: any) => {
        if (this.historialNavegacion.length <= 1) {
          this.listaArchivos();
        } else {
          this.obtenerElementos(this.historialNavegacion[this.historialNavegacion.length - 1]);
        }
        this.cerrarModal();
      },
      error => {
        console.error('Error al mover el archivo', error);
      }
    );
  }

  abrirrenombrarArchivoModal(elemento: any) {
    this.elementoSeleccionado = elemento
    if (this.esArchivo(this.elementoSeleccionado)) {
      if (this.elementoSeleccionado.lastIndexOf('.') !== -1) {
        this.nuevoNombre = this.elementoSeleccionado.slice(0, this.elementoSeleccionado.lastIndexOf('.'));
        this._modal.open(this.renombrarArchivoModal);
      }
    } else {
      this.nuevoNombre = this.elementoSeleccionado
      this._modal.open(this.renombrarArchivoModal);
    }
  }

  renombrar() {
    if (this.esArchivo(this.elementoSeleccionado)) {
      if (this.elementoSeleccionado.lastIndexOf('.') !== -1) {
        this.nuevoNombre = this.nuevoNombre + this.elementoSeleccionado.slice(this.elementoSeleccionado.lastIndexOf('.'));
      }
    }
    const original = this.historialNavegacion.slice(1).join('/') + `/${this.elementoSeleccionado}`;
    const nuevo = this.historialNavegacion.slice(1).join('/') + `/${this.nuevoNombre}`;
    this.http.post<any>('http://149.56.248.111:5057/archivo/renombrarArchivo', { original, nuevo }).subscribe(
      (resp: any) => {
        if (this.historialNavegacion.length <= 1) {
          this.listaArchivos();
        } else {
          this.obtenerElementos(this.historialNavegacion[this.historialNavegacion.length - 1]);
        }
        this.cerrarModal();
      },
      error => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Ya existe un elemento con ese nombre. Ingresa otro nombre.',
        });
      }
    );
  }


  eliminarArchivo(elemento: any) {
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'Esta acción no se puede deshacer.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        const elementoSeleccionado = this.historialNavegacion.slice(1).join('/') + `/${elemento}`;
        this.http.post<any>('http://149.56.248.111:5057/archivo/eliminarArchivo', { elementoSeleccionado }).subscribe(
          (resp: any) => {
            if (this.historialNavegacion.length <= 1) {
              this.listaArchivos();
            } else {
              this.obtenerElementos(this.historialNavegacion[this.historialNavegacion.length - 1]);
            }
            this.cerrarModal();
          },
          err => {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: 'Ya existe una carpeta con ese nombre. Ingresa otro nombre.',
            });
          }
        );
      }
    });
  }

  @HostListener('contextmenu', ['$event'])
  onRightClick(event: MouseEvent, elemento: any) {
    if (this.roleUsuario === "Superadministrador") {
      event.preventDefault();
      this.elementoSeleccionado = elemento;
      this.menuVisible = true;
      this.menuTop = `${event.clientY}px`;
      this.menuLeft = `${event.clientX}px`;
      event.stopPropagation();
    }
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: MouseEvent) {
    this.menuVisible = false;
  }

  closeMenu() {
    this.menuVisible = false;
  }

  abrirNuevaCarpetaModal() {
    this._modal.open(this.crearCarpetaModal);
  }

  crearCarpeta() {
    if (this.carpeta == "") {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Ingresa un nombre.',
      });
    } else {
      if (this.esArchivo(this.carpeta)) {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Ingresa un nombre sin extensión.',
        });
      } else {
        let aux = '';
        for (let index = 1; index < this.historialNavegacion.length; index++) {
          aux = aux + this.historialNavegacion[index] + '/'
        }
        aux = aux + this.carpeta
        this.http.post(`http://149.56.248.111:5057/archivo/crearCarpeta`, { aux }).subscribe((resp: any) => {
          if (this.historialNavegacion.length <= 1) {
            this.listaArchivos()
            this._modal.dismissAll()
          } else {
            this.obtenerElementos(this.historialNavegacion[this.historialNavegacion.length])
            this._modal.dismissAll()
          }
          this.carpeta = ''
        }, err => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Ya existe una carpeta con ese nombre. Ingresa otro nombre.',
          });
        })
      }
    }
  }

  cerrarModal() {
    this._modal.dismissAll();
  }
}