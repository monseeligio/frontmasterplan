import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRoles = route.data['expectedRoles'] as string[];

    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/']);
      return false;
    }

    const token: any = localStorage.getItem('token');
    let tokenPayload: any;

    try {
      tokenPayload = jwt_decode(token);
    } catch (err) {
      localStorage.clear();
      this.router.navigate(['/']);
      return false;
    }

    if (expectedRoles && expectedRoles.includes(tokenPayload.roleUsuario)) {
      return true;
    } else {
      this.router.navigate(['menuPrincipal']);
      return false;
    }
  }
}
