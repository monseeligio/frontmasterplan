import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { MenuPrincipalComponent } from './componentes/compartidos/menu-principal/menu-principal.component';
import { UsuariosComponent } from './componentes/superAdministrador/usuarios/usuarios.component';
import { EditarPerfilComponent } from './componentes/compartidos/editar-perfil/editar-perfil.component';
import { ReunionesComponent } from './componentes/compartidos/reuniones/reuniones.component';
import { CrearReunionComponent } from './componentes/superAdministrador/crear-reunion/crear-reunion.component';
import { ReunionComponent } from './componentes/compartidos/reunion/reunion.component';
import { ReunionIniciadaComponent } from './componentes/compartidos/reunion-iniciada/reunion-iniciada.component';
import { DocsComponent } from './componentes/compartidos/docs/docs.component';
import { TodasActividadesComponent } from './componentes/compartidos/todas-actividades/todas-actividades.component';
import { VtoComponent } from './componentes/compartidos/vto/vto.component';
import { PersonasComponent } from './componentes/compartidos/personas/personas.component';
import { EditarReunionComponent } from './componentes/superAdministrador/editar-reunion/editar-reunion.component';

import { RouteGuardService } from "./servicios/route-guard.service";

export const ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'menuPrincipal',
        component: MenuPrincipalComponent,
        canActivate: [RouteGuardService],
        data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] },
    },
    { path: 'usuarios', component: UsuariosComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'editarPerfil', component: EditarPerfilComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'reuniones', component: ReunionesComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'crearReunion', component: CrearReunionComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'editarReunion/:idReunion', component: EditarReunionComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'reunion/:idReunion', component: ReunionComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'reunionIniciada/:idReunionIniciada', component: ReunionIniciadaComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'docs', component: DocsComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'actividades', component: TodasActividadesComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'vto', component: VtoComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: 'personas', component: PersonasComponent, canActivate: [RouteGuardService], data: { expectedRoles: ['Usuario', 'Administrador', 'Superadministrador'] } },
    { path: '', pathMatch: 'full', redirectTo: 'login' },
];